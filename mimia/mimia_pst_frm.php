
	<?php	include_once("includes/cnx.inc");
	define("nOP", 0);
	define("OP_NORMAL", 1);
	define("OP_ASOCIACION", 2);
	define("OP_TIME", 3);
	define("OP_ASOCIACION_PAN", 4);
	define("OP_CONTROL", 5);
	define("OP_ASOCIACION_GW", 6);
	define("OP_FAILURE",0xff);

	/* 
	* Las variables de Mimia json:
		-camelcase guionada, castellano
		tienen notacion Identificador_Procedencia

	* Las variables de servidor:	
		-semi camelcase sin guion ingles
		tienen notacion identifyProcedence	

	*                         Objeto JSON PRINCIPAL
	* {
	*     "Id_GW"      :0001,
	*     "Pass_SRV"      :"12345abcde",              // acceso al servidor
	*     "Pass_GW"       :"12345abcde",              // acceso al GW futura la envia el GW
	*     "Secuencia"  :003,                          // SECUENCIA DEL GW 
	*     "Tk_Srv"     :0100,                         // LLAVE DE APLICACION GW-Serv
	*	  	"Tipo"	   :5,							  // tipo de trama involucrada		
	*                ARMO A CONTINUCACION SEGUN EL TIPO DE SERVICIO        
	*/

	//TODO implementar un sistema de ventana deslizante-state para autenticar token y pass en el servidor -gw de lo contrio
	//se puede perder el enlace por negacion de servicios luego de un corte o falla en internet.
	//Se cambian si y solo si la ventanas si el pass anterior es distinto, de lo contrario alguien no puede refrescar el Pass.
	$data = file_get_contents('php://input');
	echo("DUMP-RAW:> ");
	var_dump($data);
	echo("<br>");
	echo("ECHO-RAW:>".$data);
	echo("<br><br>");
	$data = $_POST['block']; //no son datos crudos
	echo("DUMP:> ");
	var_dump($data);
	echo("<br>");
	echo("ECHO:>".$data);
	echo("<br>");


	if ($data){
		/*
		* No deberia entregar informacion de falla al cliente,
		* Ante falla o demora se descarta la comunicacion.
		*/

		$pckgJsn = json_decode($data, true);  //decodifica la trama json
		echo("DUMP DECODE:> ");
		var_dump($pckgJsn);
		echo("<br>");
		$idGW	 = (int)$pckgJsn['Id_GW'];    	//Identificador de dvc GateWay MAC en asoc
		$pssSRV	 = (int)$pckgJsn['Pass_SRV']; //Key para ingresar a APP_serv	
		$pssGW	 = (int)$pckgJsn['Pass_GW'];  //Key para ingresar a APP_GW 
		$secGW	 = (int)$pckgJsn['Secuencia'];//descarto duplicacion
		$tkSrv	 = (int)$pckgJsn['Tk_Srv'];	  //Tk para ingresar al serV 	
		$typeFrame	= (int)$pckgJsn['Tipo'];
		$filaGW   = array();
		$filaMM   = array();
		$oK= false;
		/* 
		* PssSRV y tkSrv forman ambas claves dos niveles de seguridad. El token se podra usar * para decodificar AES.
		*/
		echo("<br> campo query http GateWay :".$idGW.$pssSRV.$pssGW.$secGW	.$tkSrv.$typeFrame."<br>");
		//Send the packet: T=1: Normal,T=2: Asociacion,T=3: Time,T=5:AsocGW,T=4:Control

		/******************* VALIDO EL GATEWAY **********************************/
	    if (isset($idGW)&& isset($pssSRV)&& isset($tkSrv)&& isset($secGW) && ($idGW!=0 && $pssSRV!=0 && $tkSrv!=0)){
	    	/*
	    	* Id inicial puede ser de asociacon, debo verificar si esta habilitado.
	    	* Una operacion de bit podria segmentar en la base de dato sobre los Id para *
	    	* reducir la latencia sobre cada base de datos.
	    	* La secuencia en la B.D. no debe iniciarse en 0.
	    	* EL pssSRV y tkSrv se debe asignar en fabrica al dispositivo junto con su MAC
	    	* EL pssGW se genera en el GW mediante el clock.
	    	* La secGW se genera en el GW segun el numero de intentos de comunicacion
	    	* Deberia poder levantar el GW caido con la MAC y un PSS inicial
	    	*/
			
			if($typeFrame!=OP_ASOCIACION_GW){

				/* El gateway se encuentra asociado*/

				$sSQL ="SELECT idGW,idClient,pssSRV1,tkSrv1,pssSRV2,tkSrv2
								FROM 	GateWays 
								WHERE act=1 AND BOOT=1 AND idGW=".$idGW." AND secGW!=".$secGW." 
							  			AND ( (tkSrv1=".$tkSrv." AND pssSRV1=".$pssSRV.") 
							  			OR (tkSrv2=".$tkSrv." AND pssSRV2=".$pssSRV.") )	";

				echo("<br>".$sSQL);
				$res = mysqli_query($cnx,$sSQL);	
				echo("<br>");
				var_dump($res);
				echo("<br>");
				echo(mysqli_num_rows($res));
				echo("<br>");
				//Ventana deslizante
				if($res)
				if(mysqli_num_rows($res)==1){
					/******** GW VALIDO ********/
					$filaGW = mysqli_fetch_array($res);
					echo("<br>");
					echo("cotenido respons query DB GateWay:<br>");
					var_dump($filaGW);
					echo("<br>");
					if($filaGW["pssSRV2"]==$pssSRV && $filaGW["tkSrv2"]==$tkSrv ){
						//La respuesta no llego a destino
						$tkSrv  = $filaGW["tkSrv1"];    //Corresponde al tk que no llego a destino -serv
						$pssSRV = $filaGW["pssSRV1"];   //Corresponde al pss que no llego a destino -serv
						/*En la BD queda todo como esta*/
						$sSQL = "	UPDATE 	GateWays
			                SET 		secGW= ".$secGW."
			                WHERE 	idGW = ".(int)$idGW." AND act = 1  AND BOOT= 1 ";
						$res = mysqli_query($cnx,$sSQL);						
						if($res)	$oK= true;						

					}else if($filaGW["pssSRV1"]==$pssSRV && $filaGW["tkSrv1"]==$tkSrv ){
						//Llego a destino la respuesta anterior
						$tkSrv  = rand(); 		    //Corresponde para un futuro al gw-serv
						$pssSRV = rand();    		//Corresponde para un futuro al pass gw-serv	 	
						/*comprobar que son distintos n!=(n-1)*/

						/*update GW PASS and TK*/
						$sSQL = "	UPDATE 	GateWays
		                	SET 		tkSrv1 = ".$tkSrv.",
			                				pssSRV1= ".$pssSRV.",
			                				tkSrv2 = ".$filaGW['tkSrv1'].",
			                				pssSRV2= ".$filaGW['pssSRV1'].",
			                				secGW=	 ".$secGW."
	                		WHERE 	idGW = ".(int)$idGW." AND act = 1  AND BOOT= 1 ";
						$res = mysqli_query($cnx,$sSQL);
						if($res)	$oK= true;						
					}				
				}	

			}else {

				/* El gateway no se encuentra asociado o bootea desde el reset o bootloader*/

				$sSQL ="SELECT 	idGW,
												idClient,
												gwMAC,
												pssSRV0,
												tkSrv0
					 			FROM   	GateWays
					 			WHERE  	(act=1 AND gwMAC=".$idGW." AND BOOT=0 AND tkSrv0= ".$tkSrv." AND pssSRV0= ".$pssSRV.")";

				$res = mysqli_query($cnx,$sSQL);

				if(mysqli_num_rows($res)==1){

					$filaGW = mysqli_fetch_array($res);
					$oK= true;
				}
			}
			
			//$res = mysqli_query($cnx,$sSQL);

			//El gateway es valido, se encuentra habilitado,es duplicados?
			if($oK){

				/******** GW VALIDO ********/
	        	//$fila = mysqli_fetch_array($res);
	        	//$_SESSION[registrado]=$fila[id];

				if (isset($pssGW) && isset($typeFrame) && ($pssGW!=0 )) {														
					switch ($typeFrame){ 

				    case nOP:
						/*
						*  Sin uso en esta version solo para el caso de encuesta por parte del servidor
						*/	    							
							$rJSON= array(
												"Id_GW"=>$idGW,
												"Pass_GW"=>$pssGW,   
												"Pass_SRV"=>$pssSRV,
												"Secuencia"=>$secGW,   
												"Tk_Srv"=>$tkSrv,
												"Tipo"=>nOP
											);

							echo("RESPUESTA:<br>");
							echo json_encode($rJSON);
							exit();	
							
				    break;

				    case OP_NORMAL:
				    	/*  "Tipo":1,
				         *  "Datos":{
				                     "Length":2,
				                     "Medidores":[
				                          
				                            {   "Id_M":0001,"Token":0007,"Slot":0000,"Nro_intentos":000,
				                                "Pot":00,"Noise":00,"Signal":00,"Bat":00,"DateTx":"ss:mm:hh",
				                                "MedidaRE":{"Mesure":00000,"DateMesu":"hh:dd:mm"},
				                                "MedidaDif":{"Index":03,"MedidaD":[00,00,00]} },
				                            {   "Id_M":0002,"Token":0005,"Slot":0000,"Nro_intentos":000,
				                                "Pot":00,"Noise":00,"Signal":00,"Bat":00,"DateTx":"ss:mm:hh",
				                                "MedidaRE":{"Mesure":00000,"DateMesu":"hh:dd:mm"},
				                                "MedidaDif":{"Index":03,"MedidaD":[00,00,00]} }
				                                ]
				                    } # llave de Datos        
				        *  }   # llave de elemento json HTTP         
				        */	
				    		$dat	  = $pckgJsn['Datos'];	//vector con todos los datos
				    		$lgth	  = $dat['Length'];		//longitud del vector	
				    		$measures = $dat['Medidores'];

				    		/*TODO: 
				    		*		agregar el ID del PA o punto de acceso 
				    		*		(IdMimia + IdGW + IdPAN) determin la ruta.
				    		*
				    		*/

				    		$tkM= rand(); //token que corresponde a la respuesta del bloque de respuestas Normales
					    	if (isset($measures)){

									foreach ($measures as $measure){
														
										$idMimia	= (int)$measure['Id_M'];	// pude ser erroneo, deberia ir el ID a una Black list
										$tkMimia	= (int)$measure['Token'];	// pude ser erroneo, deberia ir el ID a una Black list que informa al GW
										$trxSlot	= (int)$measure['Slot'];
										$nTries		= (int)$measure['Nro_intentos'];
										$txPower	= (int)$measure['Pot'];
										$lvlNoise	= (int)$measure['Noise'];
										$rxSignal	= (int)$measure['Signal'];
										$lvlBattery	= (int)$measure['Bat'];

										$hhhmmss=explode(':',$measure['DateTx']);  //para estado Red
						        $currentTxDate =$hhhmmss[2].":".$hhhmmss[1].":".$hhhmmss[0];
						        //838:59:59.000000  "DateTx":"ss:mm:hh",

						        $measureArr	=   	   $measure['MedidaRE'];//vector dos campos
						        $currentMeasure = (int)$measureArr['Measure'];

						        $hhddmm=explode(':',$measureArr['DateMeasu']);//Fecha de ultima medida 
						        //$hhddmm2=explode('-',date("Y-m-d"));//Fecha de ultima medida 
						        $currentMeasureDate =date("Y")."-".$hhddmm[2]."-".$hhddmm[1]." ".$hhddmm[0].":00:00";
						        //2021-12-13 03:46:14.000000 DateMesu":"hh:dd:mm"

						        $pastMeasureDif	= 	   $measure['MedidaDif'];   //vector de diferencias 2by
						        $lgthDif    =	  (int)$pastMeasureDif['Index'];     //num de medidas anteriores
						        $measuresDif=  	       $pastMeasureDif['MedidaD'];   //vec de med diferen 24hs
						        //$idMeasure		= (int)$measure['idMeasure'];
						        //$currentMeasureStatus  = $measure['estado'];
						        //$currentMeasureDescrip = $measure['observaciones'];

									  if ((isset($idMimia) && isset($tkMimia)) &&($idMimia!=0 && $tkMimia!=0)) {
											
											$sSQL = "SELECT 												
																		tkMimia1,
																		tkMimia2 
													 		FROM 	Mimias 
													 		WHERE act = 1 AND
													 					BOOT = 1 AND 
																 		idMimia=".$idMimia." AND 
																 		(tkMimia1= ".$tkMimia." OR tkMimia2= ".$tkMimia." OR tkMimia3= ".$tkMimia.")"; //tOKENS VALIDOS

											$res = mysqli_query($cnx,$sSQL);

											if(mysqli_num_rows($res)==1){

												/******** MIMIA VALIDO ********/
								        	$filaMM = mysqli_fetch_array($res);

							        	if ((isset($currentMeasure) && isset($currentMeasureDate)) &&($currentMeasure!=0)){

								        	$sSQL ="INSERT INTO 
								        					MeasureNews (
				        									idMimia,
				        									trxSlot, 								
				        									nTries,
				        									txPower,
				        									lvlNoise,
				        									rxSignal,
				        									lvlBattery,
																	currentTxDate,
																	currentMeasure,
																	currentMeasureDate,
																	measuresDif) 
													VALUES (".$idMimia.",														
															".$trxSlot.",
															".$nTries.",
															".$txPower.",
															".$lvlNoise.",
															".$rxSignal.",
															".$lvlBattery.",
															'$currentTxDate',
															".$currentMeasure.",
															'$currentMeasureDate',
															'".json_encode($pastMeasureDif)." ')";   //VECTOR DIFERENCIAS pasar a string
													echo $sSQL;
													echo "<br>";
													$res = mysqli_query($cnx,$sSQL);	
													if($res){
														$sSQL = " UPDATE	Mimias
							    			              SET 	tkMimia1 =	".$tkM.",
							        											tkMimia2 =	".$filaMM['tkMimia1'].",
							        											tkMimia3 =	".$filaMM['tkMimia2']."
							                			  WHERE idGW= ".$idGW." AND
							                			  			idMimia =	".(int)$idMimia." AND
							                			  			act = 1 AND BOOT= 1 ";
							              $res = mysqli_query($cnx,$sSQL);						                    				  		  
							              echo $sSQL;
							              echo "<br>";
							              echo $res;
							              echo "<br>Completo";
													}						              
				                }			                
						       		}else{
						        	/*agrego el Id de MIMIa al BlackList en la trama de respuesta al GateWay*/
						       		}
									  }
									}
									$tipo=OP_NORMAL;
						   		$rJSON= array(
											"Id_GW"=>$idGW,
											"Pass_GW"=>$pssGW,   
											"Pass_SRV"=>$pssSRV,
											"Secuencia"=>$secGW,   
											"Tk_Srv"=>$tkSrv,
											"Tipo"=>OP_NORMAL,										
											"tkM"=>$tkM 
										);
									echo json_encode($rJSON);
									exit();
								}else 
									echo("RESPUESTA: FALLA <br>");
						    																
							break;
						
						case OP_CONTROL:
							  // "Tipo":5,
			      //          "Id_Dvc":0000",
			      //          "Token":0000,       llave mimia->APPserv (ingreasar Serv)
			      //          "TokenMM":0000,     llave APPserv->mimia (ingreasar MIMIA)
			      //          "Subtipo":5,                         
			      //          "DatCtrl":{
			      //                      "Index": 3,                            
			      //                      "Data":[ "dato1", "dato2", "dato3" ] elementos char= 1 BYTE
			      //                     }
			      //          }  llave de elemento json HTTP             				                              

							$idDvc   = (int)$pckgJsn['Id_Dvc'];
							$tkMmSr  = (int)$pckgJsn['Token'];   //token que envia el servidor
							$tkMM 	 = (int)$pckgJsn['TokenMM']; //token enviado por el mm previo
							$subType = (int)$pckgJsn['Subtipo'];
							$datCtrl = $pckgJsn['DatCtrl'];			//vector de datos
							$lgthDif = (int)$datCtrl['Index'];  //num de bytes
						 	$MeaduresDif= $datCtrl['Data'];     //vector

						  if ((isset($currentMeasure) && isset($currentMeasureDate)) &&($idMimia!=0 && $currentMeasure!=0)) {
							
								$sSQL = "SELECT idDvc,tkMmSr 
												 FROM 	Devices 
												 WHERE  act=1 AND BOOT= 1 AND idDvc=".$idDvc." AND tkMmSr= ".$tkMmSr.""; /*busca la MAC-TOKEN*/

								$res = mysqli_query($cnx,$sSQL);		

								if($res){
									/*
									*	Respuesta acorde con la consulta
									*/
									echo json_encode("OK");

								}else{

									echo json_encode("ERR0R");

								}	
							}	

							break;
		
						case OP_TIME:

							//entra la info en la cabecera
							/*
							*	Respondo ante la consulta del gateway
							*/			

							$date=date("Y-m-d");
							$time=date("h:i:s");

							$rJSON= array(
												"Id_GW"=>$idGW,
												"Pass_GW"=>$pssGW,   
												"Pass_SRV"=>$pssSRV,
												"Secuencia"=>$secGW,   
												"Tk_Srv"=>$tkSrv,
												"Tipo"=>OP_TIME,
												"Time"=>$date."-".$time
											);

							echo("RESPUESTA TIME:<br>");
							echo json_encode($rJSON);
							exit();
							
							break;

						case OP_ASOCIACION:

							$idMimia = (int)$pckgJsn['Id_M'];  // MAC
							$tkMimia = (int)$pckgJsn['Token']; // preasignado para el volcado de dispositivos segun la MAC
							echo("<br> campo query MIMIa :".$idMimia."   ".$tkMimia."<br>");
							if ((isset($idMimia) && isset($tkMimia)) &&($idMimia!=0 && $tkMimia!=0)){
								
								$sSQL = "SELECT idMimia,										
																trxSlot,
																BOOT
												 FROM 	Mimias 
												 WHERE  idGW= ".$idGW." AND
														 		mimiaMAC= ".$idMimia." AND    
														 		tkMimia0= ".$tkMimia." AND  
														 		act=1"; 
														 		//AND BOOT=0 "; //busca la MAC-TOKEN

								$res = mysqli_query($cnx,$sSQL);							

								echo("<br>".$sSQL);								
								echo("<br>");
								var_dump($res);
								echo("<br>");
								if($res)
									echo(mysqli_num_rows($res));
								echo("<br>");

 								if($res){
									if(mysqli_num_rows($res)==1){
										/*
										*	Respuesta a corde con la consulta
										*/
										echo("ENTRA A UPDATE<br>");
										$filaMM =	mysqli_fetch_array($res);
										$tkMimia =	rand();
										if (!$filaMM['BOOT']) {																			
											$sSQL =	"	UPDATE 	Mimias 
															 	SET 		tkMimia1=".$tkMimia.", BOOT=1, onServ='".date('Y/m/d')."'
															 	WHERE  	idGW= ".$idGW." AND idMimia=".$filaMM['idMimia']." AND act=1  AND						BOOT=0";
											$res1 = mysqli_query($cnx,$sSQL);

											echo("<br>".$sSQL);								
											echo("<br>");
											var_dump($res1);
											echo("<br>");
											if($res1)
												echo(mysqli_num_rows($res));
											echo("<br>");


											/*update GW PASS and TK*/
										/*	$sSQL = " 	UPDATE 	GateWays
					                  			SET 		tkSrv = ".$tkSrv.",
								                   				secGW= 	".$secGW.",
								                   				pssSRV=	".$pssSRV."
					                  			WHERE 	idGW = 	".(int)$idGW." AND act = 1  AND BOOT= 1 ";
											$res2 = mysqli_query($cnx,$sSQL);*/
											if($res1){
												/*
												*	Respuesta acorde con la consulta
												*/										
												$rJSON= array(
														"Id_GW"=>$idGW,
														"Pass_GW"=>$pssGW,   
														"Pass_SRV"=>$pssSRV,
														"Secuencia"=>$secGW,   
														"Tk_Srv"=>$tkSrv,
														"Tipo"=>OP_ASOCIACION,										
														"Id_M"=>$idMimia,
														"Token"=>$tkMimia,
														"Slot"=>$filaMM['trxSlot'],
														"Id_Aut"=>$filaMM['idMimia'],										
														);
												echo("RESPUESTA:<br>");
												echo json_encode($rJSON);		

												exit();	
											}		   
										}else{
											/*CASO DE ASOCIACION SIN RESPUESTA ANTERIOR RECIBIDA EN EL MIMIA*/
											echo("RESPUESTA: FALLA DUPLICIDAD ASOCIACION SIN RESPUESTA<br>");
										}
									}	
										else echo("RESPUESTA: FALLA DUPLICIDAD en DB<br>");

								}else{
							    /*agrego el Id de MIMIa al BlackList en la trama de respuesta al GateWay*/
							    /*PISIBLE ATAQUE EMPLEAR UN HONEYPOOT*/
							  }
							}
								
							break;	

						case OP_ASOCIACION_PAN:

							$idMimia = (int)$pckgJsn['Id_PAN'];
							$tkMimia = (int)$measure['Token'];
							
							if ((isset($currentMeasure) && isset($currentMeasureDate)) &&($idMimia!=0 && $currentMeasure!=0)) {
							
								$sSQL ="INSERT INTO MeasureNews (idMimia, currentMeasure, currentMeasureDate) 
										VALUES (".$idMeter.", ".$currentMeasure.",  ".$currentMeasureDate.")";
								
								$res = mysqli_query($cnx,$sSQL);

								if($res){
									/*
									*	Respuesta a corde con la consulta
									*/
									echo json_encode("OK");

								}else{

									echo json_encode("ERR0R");

								}

							} else {

								echo json_encode("No hay datos");

							}	
							//exit();							
							break;									

						case OP_ASOCIACION_GW:

							/* 
							*  Los GW son caracteristicos de los clientes.
							*  varios GW indican un cliente.
							*/
							///WHERE  act=1 AND idGW='.$idGW.' AND Tk_Srv= '.$Tk_Srv.' AND pssSRV= '.$pssSRV.' AND secGW!= '.$secGW.'";			
							$tkSrv  = rand(); 		    //Corresponde para un futuro al gw-serv
							$pssSRV = rand();    		//Corresponde para un futuro al pass gw-serv								
							$sSQL = "	UPDATE 	GateWays
	                    	SET 		BOOT = 1,
	                    					tkSrv1 = ".$tkSrv.",
	                    					pssSRV1=	".$pssSRV.",
	                    					secGW= 	".$secGW."
	                    	WHERE 	gwMAC = ".(int)$idGW." AND act = 1  AND BOOT= 0 ";
								
							$res = mysqli_query($cnx,$sSQL);
							 	
							if($res){
								/*
								*	Respuesta a corde con la consulta
								*/
								$idAut=$filaGW["idGW"]; // id en tabla GateWays que corresponde a la MAC

								$rJSON= array(	
												"Id_GW"=>$idGW,
												"Pass_GW"=> $pssGW,
												"Pass_SRV"=>$pssSRV,
												"Secuencia"=>$secGW,
												"Tk_Srv"=>$tkSrv,
												"Tipo"=>OP_ASOCIACION_GW,
												"Id_Aut"=>$idAut);   
								echo("RESPUESTA:<br>");
								echo json_encode($rJSON);
								unset($GLOBALS[_SERVER]);
	unset($_REQUEST);	
	$_SERVER['PHP_AUTH_USER']="";
								exit();	
							}
							
							break;
					}	
				}
			}
		}					
	    echo ("No se han recibido TODOS los datos");

		//$rJSON= array ("Id_GW"=>11,"Pass_GW"=>12,"Pass_SRV"=>13,"Secuencia"=>14,"Tk_Srv"=>15,"Tipo"=>OP_NORMAL,"tkM"=>16);

		//echo json_encode($rJSON); 

	unset($_REQUEST);	
	$_SERVER['PHP_AUTH_USER']="";
	    exit();
	} else {

		//echo json_encode("");
		echo ("<br> No se han recibido datos");

		 //$rJSON= array ("Id_GW"=>11,"Pass_GW"=>12,"Pass_SRV"=>13,"Secuencia"=>14,"Tk_Srv"=>15,"Tipo"=>OP_NORMAL,"tkM"=>16);

		//echo json_encode($rJSON);
		//unset($GLOBALS[_SERVER]);
	unset($_REQUEST);	
	$_SERVER['PHP_AUTH_USER']="";

		exit();
	}

	echo ("<br> Fuera de protocolo");
	unset($GLOBALS[_SERVER]);
	unset($_REQUEST);	
	$_SERVER['PHP_AUTH_USER']="";
	exit();
		
	?>