<?php
class Utils{
	private $numeros =    array("-", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve");
	private $numerosX =   array("-", "un", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve");
	private $numeros100 = array("-", "ciento", "doscientos", "trecientos", "cuatrocientos", "quinientos", "seicientos", "setecientos", "ochocientos", "novecientos");
	private $numeros11 =  array("-", "once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve");
	private $numeros10 =  array("-", "-", "-", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa");

	public function __construct(){
		
	}
	
	public function pageWhitOutBar($sPage,$cnx){

		$res = mysqli_query($cnx,"SELECT count(*) AS total FROM without_bar WHERE `page` = '$sPage'");
		if($res){
			$row = mysqli_fetch_assoc($res);
			if((int)$row['total'] >= 1){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	
	// pasar n�mero a letras

function numtoletras($xcifra)
{
    $xarray = array(0 => "Cero",
        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
    );
//
    $xcifra = trim($xcifra);
    $xlength = strlen($xcifra);
    $xpos_punto = strpos($xcifra, ".");
    $xaux_int = $xcifra;
    $xdecimales = "00";
    if (!($xpos_punto === false)) {
        if ($xpos_punto == 0) {
            $xcifra = "0" . $xcifra;
            $xpos_punto = strpos($xcifra, ".");
        }
        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
    }
 
    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
    $xcadena = "";
    for ($xz = 0; $xz < 3; $xz++) {
        $xaux = substr($XAUX, $xz * 6, 6);
        $xi = 0;
        $xlimite = 6; // inicializo el contador de centenas xi y establezco el l�mite a 6 d�gitos en la parte entera
        $xexit = true; // bandera para controlar el ciclo del While
        while ($xexit) {
            if ($xi == $xlimite) { // si ya lleg� al l�mite m�ximo de enteros
                break; // termina el ciclo
            }
 
            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres d�gitos)
            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
                switch ($xy) {
                    case 1: // checa las centenas
                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres d�gitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                             
                        } else {
                            $key = (int) substr($xaux, 0, 3);
                            if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es n�mero redondo (100, 200, 300, 400, etc..)
                                $xseek = $xarray[$key];
                                $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Mill�n, Millones, Mil o nada)
                                if (substr($xaux, 0, 3) == 100)
                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                            }
                            else { // entra aqu� si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                $key = (int) substr($xaux, 0, 1) * 100;
                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                $xcadena = " " . $xcadena . " " . $xseek;
                            } // ENDIF ($xseek)
                        } // ENDIF (substr($xaux, 0, 3) < 100)
                        break;
                    case 2: // checa las decenas (con la misma l�gica que las centenas)
                        if (substr($xaux, 1, 2) < 10) {
                             
                        } else {
                            $key = (int) substr($xaux, 1, 2);
                            if (TRUE === array_key_exists($key, $xarray)) {
                                $xseek = $xarray[$key];
                                $xsub = $this->subfijo($xaux);
                                if (substr($xaux, 1, 2) == 20)
                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                $xy = 3;
                            }
                            else {
                                $key = (int) substr($xaux, 1, 1) * 10;
                                $xseek = $xarray[$key];
                                if (20 == substr($xaux, 1, 1) * 10)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                            } // ENDIF ($xseek)
                        } // ENDIF (substr($xaux, 1, 2) < 10)
                        break;
                    case 3: // checa las unidades
                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
                             
                        } else {
                            $key = (int) substr($xaux, 2, 1);
                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
                            $xsub = $this->subfijo($xaux);
                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                        } // ENDIF (substr($xaux, 2, 1) < 1)
                        break;
                } // END SWITCH
            } // END FOR
            $xi = $xi + 3;
        } // ENDDO
 
        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
            $xcadena.= " DE";
 
        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
            $xcadena.= " DE";
 
        // ----------- esta l�nea la puedes cambiar de acuerdo a tus necesidades o a tu pa�s -------
        if (trim($xaux) != "") {
            switch ($xz) {
                case 0:
                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                        $xcadena.= "UN BILLON ";
                    else
                        $xcadena.= " BILLONES ";
                    break;
                case 1:
                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                        $xcadena.= "UN MILLON ";
                    else
                        $xcadena.= " MILLONES ";
                    break;
                case 2:
                    if ($xcifra < 1) {
                        $xcadena = "CERO PESOS $xdecimales/100";
                    }
                    if ($xcifra >= 1 && $xcifra < 2) {
                        $xcadena = "UN PESO $xdecimales/100";
                    }
                    if ($xcifra >= 2) {
                        $xcadena.= " PESOS $xdecimales/100"; //
                    }
                    break;
            } // endswitch ($xz)
        } // ENDIF (trim($xaux) != "")
        // ------------------      en este caso, para M�xico se usa esta leyenda     ----------------
        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
    } // ENDFOR ($xz)
    return trim($xcadena);
}
 
// END FUNCTION
 
function subfijo($xx)
{ // esta funci�n regresa un subfijo para la cifra
    $xx = trim($xx);
    $xstrlen = strlen($xx);
    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
        $xsub = "";
    //
    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
        $xsub = "MIL";
    //
    return $xsub;
}
// fin pasar n�meros a letras
			
			
			
public function sendEmail($vDest,$sTitle,$sMessage,$file){
		
		require_once("includes/email/class.phpmailer.php");
		require_once("includes/email/class.smtp.php");

			$mail = new PHPMailer();
			$mail->IsSMTP();
			
			$mail->SMTPSecure = 'tls';
            $mail->Host = "smtp.gmail.com";
            $mail->Port = "587"; 
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = true;
			
			
			
		//	$mail->SMTPDebug = 0;
		//	$mail->Host = "smtp.gmail.com";
		//	$mail->Port = "465"; 
		//	$mail->SMTPSecure = 'ssl';
		//	$mail->SMTPAuth = true;
			$mail->Username = "info@it-whatsescort.com";
			$mail->Password = "Arianna1709";
			$mail->From = "info@it-whatsescort.com";
			//$mail->Timeout      = 10;
		    //$mail->AddEmbeddedImage('assets/layouts/layout5/img/logo.png', 'logo');	
			$mail->FromName = "WhatsEscort";
			
			
			
			
			
			$mail->Subject = $sTitle;
			$mail->AddAddress($vDest);
                    
                     		$body = '<!doctype html>
<html>
<head><meta charset="windows-1252">

<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Use the latest (edge) version of IE rendering engine -->
<title>WhatsEscort</title>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<style type="text/css">
@media screen {
  @font-face {
    font-family: "Lato";
    font-style: normal;
    font-weight: 400;
    src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");
  }
  </style>
<style type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet" type="text/css" />
@import url("https://fonts.googleapis.com/css?family=Montserrat");
@font-face{font-family:"Montserrat";font-style:normal;font-weight:400;src:local("Montserrat"),local(Montserrat),url(http://fonts.gstatic.com/s/montserrat/v10/gFXtEMCp1m_YzxsBpKl68iEAvth_LlrfE80CYdSH47w.woff2) format("woff2");unicode-range:U+0100-024F,U+1E00-1EFF,U+20A0-20AB,U+20AD-20CF,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:"Montserrat";font-style:normal;font-weight:400;src:local("Montserrat Regular"),local(Montserrat-Regular),url(http://fonts.gstatic.com/s/montserrat/v10/zhcz-_WihjSQC0oHJ9TCYPk_vArhqVIZ0nv9q090hN8.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2212,U+2215}@font-face{font-family:"Montserrat";
font-style:normal;font-weight:700;src:local("Montserrat Bold"),local(Montserrat-Bold),
url(http://fonts.gstatic.com/s/montserrat/v10/IQHow_FEYlDC4Gzy_m8fcjh33M2A-6X0bdu871ruAGs.woff2) format("woff2");unicode-range:U+0102-0103,U+1EA0-1EF9,U+20AB}@font-face{font-family:"Montserrat";font-style:normal;font-weight:700;src:local("Montserrat Bold"),local(Montserrat-Bold),url(http://fonts.gstatic.com/s/montserrat/v10/IQHow_FEYlDC4Gzy_m8fchHJTnCUrjaAm2S9z52xC3Y.woff2) format("woff2");unicode-range:U+0100-024F,U+1E00-1EFF,U+20A0-20AB,U+20AD-20CF,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:"Montserrat";font-style:normal;font-weight:700;src:local("Montserrat Bold"),local(Montserrat-Bold),url(http://fonts.gstatic.com/s/montserrat/v10/IQHow_FEYlDC4Gzy_m8fcoWiMMZ7xLd792ULpGE4W_Y.woff2) format("woff2");
unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2212,U+2215}
/* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
	@import url("https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700");*/  
	  html, body {
	margin: 0 !important;
	padding: 0 !important;
	height: 100% !important;
	width: 100% !important;
	 font-family: "Lato !important";
}
/* What it does: Stops email clients resizing small text. */
* {
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
}
/* What it does: Forces Outlook.com to display emails full width. */
.ExternalClass {
	width: 100%;
}
/* What is does: Centers email on Android 4.4 */
div[style*="margin: 16px 0"] {
	margin: 0 !important;
}
/* What it does: Stops Outlook from adding extra spacing to tables. */
table, td {
	mso-table-lspace: 0pt !important;
	mso-table-rspace: 0pt !important;
}
/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
table {
	border-spacing: 0 !important;
	border-collapse: collapse !important;
	table-layout: fixed !important;
	margin: 0 auto !important;
}
table table table {
	table-layout: auto;
}
/* What it does: Uses a better rendering method when resizing images in IE. */
img {
	-ms-interpolation-mode: bicubic;
}
.yshortcuts a {
	border-bottom: none !important;
}
/* What it does: Another work-around for iOS meddling in triggered links. */
a[x-apple-data-detectors] {
	color: inherit !important;
}
</style>
<!-- Progressive Enhancements -->
<style type="text/css" scoped>
/* What it does: Hover styles for buttons */
.button-td, .button-a {
	transition: all 100ms ease-in;
}
.button-td:hover, .button-a:hover {
	background: #00476A !important;
	border-color: #00476A!important;
}
   td.textcenter
   {
	   text-align:right !important;
	   
   }
/* Media Queries */
	@media screen and (max-width: 550px) {
   td.textcenter
   {
	   text-align:center !important;
	   
   }
	table.buttonclick
	{
		width:100% !important;
		
		 text-align:center !important;
	}
	}
</style>
<!--[if mso]>
<style type="text/css">
.fallback-font {
font-family: sans-serif;
}
</style>
<![endif]-->
</head>
<body width="100%" style="margin: 0;" yahoo="yahoo">
<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
  <tr>
    <td><center style="width: 100%;">
        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;"><?php echo $texto_previo;?></div>
        <!-- Visually Hidden Preheader Text : END -->
        <div style="max-width: 680px;"> 
          <!-- Email Header : BEGIN -->
          <table cellspacing="0" cellpadding="0" border="0" align="center" >
            <tr>
              <td style="padding: 15px 50px; text-align: center">
			  <a href="https://it-whatsescort.com">
			  <img width="25%" src="https://it-whatsescort.com/img/logo.png"  border="0"></a></td>
			  </tr>
          </table>
          <!-- Email Header : END --> 
          <!-- Email Body : BEGIN -->
          <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" style="max-width: 680px;">
          <!-- Background Image with Text : BEGIN -->
            <tr>
              <td bgcolor="#85d3d7" valign="middle" style="text-align: center; background-size:20%;background-position: right 35px center; background-repeat: no-repeat; padding:10px"><!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:680px;height:175px; background-position: center center !important;">
                        <v:fill type="tile" src="assets/Hybrid/Image_680x230.png" color="#22979e" />
                        <v:textbox inset="0,0,0,0">
                        <![endif]-->';
						
						
						$body .= '   <div> 
<!--[if mso]>
                            <table border="0" cellspacing="0" cellpadding="0" align="center" width="500">
                            <tr>
                            <td align="center" valign="top" width="500">
                            <![endif]-->
							<div style="display: none; max-height: 0px; overflow: hidden;">.&nbsp; Ciao, tu preinscripci�n fue exitosa :.&nbsp;  '.$sMessage.'
</div>

<!-- Insert &zwnj;&nbsp; hack after hidden preview text -->
<div style="display: none; max-height: 0px; overflow: hidden;">
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;

</div>
                  <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:680px; margin: auto;">
                    <tr>
                      <td valign="middle" alt="" style="text-align: left; padding: 40px 25px;font-size: 15px; mso-height-rule: exactly; color: #ffffff;" class="fallback-font"  > <span  style="font-size: 4.7vmin;">
					  BEN FATTO! CI SEI QUASI...</span></td>
                    </tr>
                  </table>
                  <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]--> 
                </div>
                
                <!--[if gte mso 9]>
                        </v:textbox>
                        </v:rect>
                        <![endif]--></td>
            </tr>
            <!-- Background Image with Text : END -->              
            <!-- 1 Column Text : BEGIN -->
            <tr>
              <td><table cellspacing="0" cellpadding="0" border="0" width="100%">
                  <tr>
                    <td style="padding: 35px 35px 35px; text-align: left; font-family:Montserrat Medium !important; font-size: 16px; background-color: #f0f1f3; mso-height-rule: exactly; line-height: 24px; color: #22979e; border-bottom: 1px solid #ccc;"><h1 style="margin-bottom: 5px; font-size: 24px;font-family:Montserrat Medium !important; line-height: 28px; font-weight: 400;">II tuo annuncio non � ancora pubblicato </h1><br>
                      '.$sMessage.' <br><br>
					  <span style="font-family:Montserrat Medium !important; font-style:italic !important;">
"Buona Pubblicazione" 
.</span><br><br>
                      <br>
                      <!-- Button : Begin -->
                      
                      <table cellspacing="0" cellpadding="0" border="0" class="buttonclick"  align="right" style="margin: auto">
                        <tr>
                          <td style="border-radius: 3px; background: #008f90; text-align: right;" class="button-td buttonclick"><a href="https://it-whatsescort.com/" style="background: #008f90;; border: 10px solid #008f90; padding: 0 10px;color: #ffffff; font-family:Montserrat Semibold !important; font-size: 12px;letter-spacing:.12em;  text-align: center; text-decoration: none; display: block; border-radius: 3px;" class="button-a textcenter"> 
                            <!--[if mso]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]-->it-whatsescort.com<!--[if mso]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]--> 
                            </a></td>
                        </tr>
                      </table>
                      
                      <!-- Button : END --></td>
                  </tr>
                </table></td>
</tr>';
						
   $body .='
          </table>
          <!-- Email Body : END --> 
          <!-- Email Footer : BEGIN -->
          <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
        <tr>
			 <td  valign="bottom" class="textcenter" style="padding: 20px 2px;  font-size:
			 16px; mso-height-rule: exactly;			
			 line-height: 18px; color: #7a8996;"> 
<!--[if (mso)]>
    <table "width:100%">
	<tr>
	<td align="center">
<![endif]-->			 
				 
<!--[if (mso) | ]>
    </td>
	</tr>
	</table>
<![endif]-->			 			 
			 </td>
        </tr>
			</table>    
          <!-- Email Footer : END --> 
        </div>
      </center></td>
  </tr>
</table>
</body>
</html>';
					
					if ($file && !is_null($file)){
			//var_dump($file);exit;
			//$doc='liquidation/'.$file;
			$mail->addAttachment($file);
			}
			
			

			$mail->Body = $body;
			$mail->IsHTML(true);
						
if(!$mail->Send()) {

return $mail->ErrorInfo;

} else {

return true;
	  }
		
			
	}
	
	public function getRetrievePasswordMessage($tpl,$sURLToOpenMail,$sCustomerName,$sUserName,$sUserPassword){
		$tpl->load_file("mail/retrievePassword.html","main");
		$tpl->set_var("sURLToOpenMail",$sURLToOpenMail);
		$tpl->set_var("sCustomerName",$sCustomerName);
		$tpl->set_var("sUserName",$sUserName);
		$tpl->set_var("sUserPassword",$sUserPassword);
		$tpl->parse("main",false);
		return $tpl->getParsedVar("main");
	}
	
	public function sumaDia($fecha,$dia){ //Formato dd/mm/AAAA
		list($day,$mon,$year) = explode('/',$fecha);
		return date('Y-m-d',mktime(0,0,0,$mon,$day+$dia,$year));
	} 

	public function checkData($oData){
		foreach($oData as $sField => $sValue){
			
			if(is_null($sValue) || (string)$sValue == ""){
				return false;
				exit;
			}
		}
		return true;
	} 
	
	
	public function validarCampos($oData,$rePass,$user){
		$oResult = new stdClass();
		if ($oData->password === $rePass) {
			$nameUser = $user->checkUser($oData->userName,$oData->email);
			if ($nameUser === FALSE){
				if (!filter_var($oData->email, FILTER_VALIDATE_EMAIL)) {
					$oResult->Status = "Invalid email format"; 
				} else {
					$oResult->Status = "OK";
				}
			} else {
				$oResult->Status = 'Nombre de Usuario o Email ya en uso';
			}
		} else {
			$oResult->Status = 'Las contrase&ntilde;as no coinciden';
		}
		return $oResult;
	} 
	
	
	
	
	
	
	
	public function getUserTypes($tpl,$user,$userType,$sPrefix){
		$vData = $user->getUserTypes();
		foreach($vData as $Item){
			if($userType == $Item->typeID){
				$tpl->set_var("sSelected","selected='selected'");
			}else{
				$tpl->set_var("sSelected","");		
			}
			$tpl->set_var("sUserType",$Item->typeName);
			$tpl->set_var("userTypeID",$Item->typeID);

			$tpl->set_var("sNewUserType",$Item->typeName);
			$tpl->set_var("newUserTypeID",$Item->typeID);

			$tpl->set_var("sEditUserType",$Item->typeName);
			$tpl->set_var("editUserTypeID",$Item->typeID);

			$tpl->parse("UserTypeBlock",true);
			$tpl->parse("NewUserTypeBlock",true);
			$tpl->parse("EditUserTypeBlock",true);
		}
	}
	
		public function getUserTypeDocument($tpl,$user){
		$vData = $user->getUserTypeDocument();
		foreach($vData as $Item){
			
			//$tpl->set_var("sUserTypeDocument",$Item->typeName);
			//$tpl->set_var("userTypeDocumentID",$Item->typeID);

			$tpl->set_var("sNewUserTypeDocument",$Item->typeName);
			$tpl->set_var("newUserTypeDocumentID",$Item->typeID);

			$tpl->set_var("sEditUserTypeDocument",$Item->typeName);
			$tpl->set_var("editUserTypeDocumentID",$Item->typeID);

			//$tpl->parse("UserTypeDocumentBlock",true);
			$tpl->parse("NewUserTypeDocumentBlock",true);
			$tpl->parse("EditUserTypeDocumentBlock",true);
		}
	}
	
	public function getUserStatus($tpl,$user){
		$vData = $user->getUserStatus();
		foreach($vData as $Item){
			if($Item->statusID != 1){ //No se muestra el estado eliminado en el alta
				$tpl->set_var("sNewUserStatus",$Item->statusName);
				$tpl->set_var("newUserStatusID",$Item->statusID);
				$tpl->parse("NewUserStatusBlock",true);

				$tpl->set_var("sEditUserStatus",$Item->statusName);
				$tpl->set_var("editUserStatusID",$Item->statusID);
				$tpl->parse("EditUserStatusBlock",true);
			}/*else{
				$tpl->set_var("sEditUserStatus",$Item->statusName);
				$tpl->set_var("editUserStatusID",$Item->statusID);
				$tpl->parse("EditUserStatusBlock",true);
			}*/
		}
	}

	public function getUserCategories($tpl,$user){
		$vData = $user->getUserCategories();
		foreach($vData as $Item){
			$tpl->set_var("sNewUserCategory",$Item->categoryName);
			$tpl->set_var("newUserCategoryID",$Item->categoryID);
			$tpl->parse("NewUserCategoryBlock",true);

			$tpl->set_var("sEditUserCategory",$Item->categoryName);
			$tpl->set_var("editUserCategoryID",$Item->categoryID);
			$tpl->parse("EditUserCategoryBlock",true);
		}
	}

	public function getCondominiusTypes($tpl,$condominium){
		$vData = $condominium->getCondominiumTypes();
		foreach($vData as $Item){
			$tpl->set_var("sCondominiumType",$Item->condominiumName);
			$tpl->set_var("condominiumTypeID",$Item->condominiumID);
			$tpl->parse("CondominiumTypeBlock",true);

			$tpl->set_var("sEditCondominiumType",$Item->condominiumName);
			$tpl->set_var("editCondominiumTypeID",$Item->condominiumID);
			$tpl->parse("EditCondominiumTypeBlock",true);
		}
	}
	public function getCondominiumDataTypes($tpl,$condominium){
		$vData = $condominium->getCondominiumDataTypes();
		foreach($vData as $Item){
			$tpl->set_var("sCondominiumData",$Item->condominiumDataName);
			$tpl->set_var("condominiumDataID",$Item->condominiumDataID);
			$tpl->parse("CondominiumDataBlock",true);

			$tpl->set_var("sEditCondominiumData",$Item->condominiumDataName);
			$tpl->set_var("editCondominiumDataID",$Item->condominiumDataID);
			$tpl->parse("EditCondominiumDataBlock",true);
		}
	}
	public function getCondominiumStorages($tpl,$condominium){
		$vData = $condominium->getCondominiumStorages();
		foreach($vData as $Item){
			$tpl->set_var("sCondominiumStorage",$Item->condominiumStorageName);
			$tpl->set_var("condominiumStorageID",$Item->condominiumStorageID);
			$tpl->parse("CondominiumStorageBlock",true);

			$tpl->set_var("sEditCondominiumStorage",$Item->condominiumStorageName);
			$tpl->set_var("editCondominiumStorageID",$Item->condominiumStorageID);
			$tpl->parse("EditCondominiumStorageBlock",true);
		}
	}
	public function getProvinces($tpl,$db){
		$vData = $db->getProvinces();
		foreach($vData as $Item){
			$tpl->set_var("sNewUserProvince",$Item->name);
			$tpl->set_var("newUserProvinceID",$Item->id);
			$tpl->parse("NewUserProvinceBlock",true);

			$tpl->set_var("sEditUserProvince",$Item->name);
			$tpl->set_var("editUserProvinceID",$Item->id);
			$tpl->parse("EditUserProvinceBlock",true);
		}
	}
	
	public function getMeterStatus($tpl,$meter,$iMeterStatus){
		$vData = $meter->getMeterStatus();
		foreach($vData as $Item){
/*			if($iMeterStatus == (int)$Item['id']){
				$tpl->set_var("sEditMeterStatusSelected","selected='selected'");
			}else{
				$tpl->set_var("sEditMeterStatusSelected","");		
			}
*/
			$tpl->set_var("sEditMeterStatus",$Item['description']);
			$tpl->set_var("iEditMeterStatusId",$Item['id']);
			$tpl->parse("EditMeterStatusBlock",true);

			if($Item['id'] != 5 && $Item['id'] != 6){
				$tpl->set_var("sNewMeterStatus",$Item['description']);
				$tpl->set_var("iNewMeterStatusId",$Item['id']);
				$tpl->parse("NewMeterStatusBlock",true);
			}
		}
	}

	public function getPropertyTypes($tpl,$property){
		$vData = $property->getPropertyTypes();
		foreach($vData as $Item){
			$tpl->set_var("sEditMeterPropertyType",$Item['description']);
			$tpl->set_var("iEditMeterPropertyTypeId",$Item['id']);
			$tpl->set_var("sNewMeterPropertyType",$Item['description']);
			$tpl->set_var("iNewMeterPropertyTypeId",$Item['id']);

			$tpl->parse("EditMeterPropertyTypeBlock",true);
			$tpl->parse("NewMeterPropertyTypeBlock",true);
		}
	}

	public function getPropertyRates($tpl,$property,$edit=true){
		$vData = $property->getPropertyRates();
		foreach($vData as $Item){
		    if ($edit) {
					$tpl->set_var("sEditMeterRateType",$Item['description']);
					$tpl->set_var("iEditMeterRateTypeId",$Item['id']);
			}
			
			$tpl->set_var("sNewMeterRateType",$Item['description']);
			$tpl->set_var("iNewMeterRateTypeId",$Item['id']);
			
              if ($edit) {
							$tpl->parse("EditMeterRateTypeBlock",true);
			  }         
			
			$tpl->parse("NewMeterRateTypeBlock",true);
		}
	}

	public function getPropertyImage($property,$iPropertyId){
		$vData = $property->getImage($iPropertyId);
		$tpl->set_var("sEditMeterPropertyImage",$Item['id']);

	}
	
	public function getDocumentation($tpl,$user) {
	$vData = $user->getDocumentationTypes();
		foreach($vData as $Item){
			$tpl->set_var("sDocDescription",$Item->description);
			$tpl->set_var("iDocId",$Item->id);
			$tpl->parse ("EmptyDocTypeBlock",true);
			$tpl->set_var("sEditDocDescription",$Item->description);
			$tpl->set_var("iEditDocId",$Item->id);
			$tpl->parse ("EditEmptyDocTypeBlock",true);
		}
	}
	
	
		public function getMeterUserCategories($tpl,$user){
		$vData = $user->getUserCategories();
		foreach($vData as $Item){
			$tpl->set_var("sNewMeterCategory",$Item->categoryName);
			$tpl->set_var("newMeterCategoryID",$Item->categoryID);
			$tpl->parse("NewMeterCategoryBlock",true);

			$tpl->set_var("sEditMeterCategory",$Item->categoryName);
			$tpl->set_var("editMeterCategoryID",$Item->categoryID);
			$tpl->parse("EditMeterCategoryBlock",true);
		}
	}
	
	
		public function getMeterUserTypeDocument($tpl,$user){
		$vData = $user->getUserTypeDocument();
		foreach($vData as $Item){
			$tpl->set_var("sNewMeterTypeDocument",$Item->typeName);
			$tpl->set_var("newMeterTypeDocumentID",$Item->typeID);
			$tpl->parse("NewMeterTypeDocumentBlock",true);

			$tpl->set_var("sEditMeterTypeDocument",$Item->typeName);
			$tpl->set_var("editMeterTypeDocumentID",$Item->typeID);
			$tpl->parse("EditMeterTypeDocumentBlock",true);
		}
	}

	public function showUserSearchForm($db,$user,$condominium,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $iUserType, $vPOST){
		// USERS TYPES
		$this->getUserTypes($tpl,$user,$iUserType,null);
		// TYPE DOCUMENT
		$this->getUserTypeDocument($tpl,$user);
		// USERS STATUS
		$this->getUserStatus($tpl,$user);
		// USERS CATEGORIES
		$this->getUserCategories($tpl,$user);
		// CONDOMINIUMS TYPES
		$this->getCondominiusTypes($tpl,$condominium);
		// CONDOMINIUMS DATA TYPES
		$this->getCondominiumDataTypes($tpl,$condominium);
		// CONDOMINIUMS DATA STORAGE
		$this->getCondominiumStorages($tpl,$condominium);
		// PROVINCES
		$this->getProvinces($tpl,$db);
		// DOCUMENTACION
		$this->getDocumentation($tpl,$user);

		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("dUserDischarge",date("d/m/Y"));
		$tpl->set_var("userId",$vPOST['userID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("iCUIT",$vPOST['userCUIT']);
		$tpl->set_var("userName",$vPOST['userName']);
		$tpl->set_var("userLastName",$vPOST['userLastName']);
		$tpl->set_var("sUserAddress",$vPOST['userAddress']);
		$tpl->set_var("sUserCity",$vPOST['userCity']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	
	
		public function showUserAdminSearchForm($db, $user,$tpl, $iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST){
		// USERS TYPES
		//$this->getUserTypes($tpl,$user,$iUserType,null);
		// USERS STATUS
		//$this->getUserStatus($tpl,$user);
        
		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("userAdminId",$vPOST['userAdminID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("userAdminName",$vPOST['userAdminName']);
		$tpl->set_var("userAdminLastName",$vPOST['userAdminLastName']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	
	
	
	public function showConceptSearchForm($db,$concept,$property,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $iConceptType,$iConceptOrder, $vPOST,$iConceptSalePoint){

		// CONCEPT TYPES
		$this->getConceptTypes($tpl,$concept,$iConceptType,null);
		// CONCEPT SALEPOINT
		$this->getConceptSalePoint($tpl,$concept,$iConceptSalePoint,null);
        // CONCEPT RATES
		$this->getConceptRates($tpl,$property);


		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("dConceptDischarge",date("d/m/Y"));
		$tpl->set_var("conceptId",$vPOST['conceptID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("conceptName",$vPOST['conceptName']);
		
		if(!is_null($vPOST['conceptOrder'])&& $vPOST['conceptOrder']!="" ){
		              if ($vPOST['conceptOrder']==1) {
				$tpl->set_var("sSelectedOrder1","selected='selected'");
				} else {
				$tpl->set_var("sSelectedOrder2","selected='selected'");
				}
			}else{
				$tpl->set_var("sSelectedOrder0","selected='selected'");		
			}
			
		
		
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	
	
	public function showPropertySearchForm($db,$property,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage,$vPOST){


		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("Id",$vPOST['ID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("Name",$vPOST['Name']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}


	public function showSearchForm($tpl,$pay,$payType,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST,$billYear,$bill){

		// FORMAS DE PAGO
		$this->getSummaryPaymentTypes($tpl,$pay,$payType,null);
	// BILL YEARS
		$this->getBillYear($tpl,$bill,$billYear,null);
		

		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	


		public function getSummaryPaymentTypes($tpl,$pay){
		$vData = $pay->getMethodPayment();
		foreach($vData as $Item){
		    $tpl->set_var("sNewSummaryPayment",$Item['description']);
			$tpl->set_var("newSummaryPaymentID",$Item['id']);
			

		$tpl->parse("NewSummaryPaymentBlock",true);
		}
	}

	
	public function getBillYear($tpl,$bill){
		$vData = $bill->getBillYear();
		foreach($vData as $Item){
		    $tpl->set_var("sBillYear",$Item['year']);
		$tpl->parse("BillYearBlock",true);
		}
	}
	
	
	
		public function showMethodPaymentSearchForm($db,$pay,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage,$vPOST){


		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("Id",$vPOST['ID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("Name",$vPOST['Name']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}
	
	public function showCategoriesSearchForm($db,$property,$tpl,$iTotPages, $iRecPerPage, $iMinLimit, $iPage,$vPOST){


		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		
		$tpl->set_var("Id",$vPOST['ID']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("Name",$vPOST['Name']);
		$tpl->set_var("Percent",$vPOST['percent']);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}	

	
	public function showConfigSystem($db,$configSystem,$tpl){
//para cargar las provincias
		// PROVINCES
		$vData = $db->getProvinces();
		$oData = $configSystem->getConfigSystem();
		
// para ver la provincia que tiene cargada
		foreach($vData as $Item){
		    $tpl->set_var("configSystemProvinceID",$Item->id);
			$tpl->set_var("configSystemProvince",$Item->name);
			if ($oData->province == $Item->id) {
				$tpl->set_var("systemProvinceSelected","selected='selected'");
				//para la ciudad
				// Ciudad
				$vData1 = $db->getCitiesByProvince($oData->province);
				$oData1 = $configSystem->getConfigSystem();
			}else{
				$tpl->set_var("systemProvinceSelected","");
			}
			$tpl->parse("ConfigSystemProvinceBlock",true);
		}	
// para ver la ciudad que tiene cargada
		foreach($vData1 as $Item1){
		    $tpl->set_var("configSystemCityID",$Item1->id);
			$tpl->set_var("configSystemCity",$Item1->name);
			if ($oData1->city == $Item1->id) {
				$tpl->set_var("systemCitySelected","selected='selected'");
			}else{
				$tpl->set_var("systemCitySelected","");
			}
			$tpl->parse("ConfigSystemCityBlock",true);
		}	
		
		
	    $tpl->set_var("configSystemName",$oData->name);
		$tpl->set_var("configSystemCuit",$oData->cuit);
		$tpl->set_var("configSystemAddress",$oData->address);
		$tpl->set_var("configSystemZip",$oData->zip);
		$tpl->set_var("configSystemLocation",$oData->location);
		$tpl->set_var("configSystemEmail",$oData->email);
		$tpl->set_var("configSystemWeb",$oData->web);
		$tpl->set_var("configSystemPointSaleWater",$oData->pointSaleWater);
		$tpl->set_var("configSystemPointSaleRent",$oData->pointSaleRent);
		$tpl->set_var("configSystemPointSalePark",$oData->pointSalePark);
		$tpl->set_var("configSystemPointSaleConection",$oData->pointSaleConection);
        $tpl->set_var("configSystemPublic",$oData->public_opening);
        $tpl->set_var("configSystemPhone",$oData->phone);
        $tpl->set_var("configSystemFirstDay",$oData->firstday);
        $tpl->set_var("configSystemSecondDay",$oData->secondday);
		
		$tpl->set_var("configSystemSeat",$oData->seat);
		$tpl->set_var("configSystemInvoice",$oData->invoice);
		$tpl->set_var("configSystemInterests",$oData->interests);
		
		$tpl->set_var("configSystemM3",$oData->m3);
		$tpl->set_var("configSystemM31",$oData->m31);
		$tpl->set_var("configSystemM32",$oData->m32);
		$tpl->set_var("configSystemM33",$oData->m33);
	   $tpl->set_var("configSystemNumberWaterA",$oData->numberWaterA);
	   $tpl->set_var("configSystemNumberWaterB",$oData->numberWaterB);
	   $tpl->set_var("configSystemNumberNoteWaterA",$oData->numberNoteWaterA);
	   $tpl->set_var("configSystemNumberNoteWaterB",$oData->numberNoteWaterB);
	   $tpl->set_var("configSystemNumberDebitWaterA",$oData->numberDebitWaterA);
	   $tpl->set_var("configSystemNumberDebitWaterB",$oData->numberDebitWaterB);
	   
	    $tpl->set_var("configSystemNumberConectionA",$oData->numberConectionA);
	   $tpl->set_var("configSystemNumberConectionB",$oData->numberConectionB);
	   $tpl->set_var("configSystemNumberNoteConectionA",$oData->numberNoteConectionA);
	   $tpl->set_var("configSystemNumberNoteConectionB",$oData->numberNoteConectionB);
	   $tpl->set_var("configSystemNumberDebitConectionA",$oData->numberDebitConectionA);
	   $tpl->set_var("configSystemNumberDebitConectionB",$oData->numberDebitConectionB);
	   
	    $tpl->set_var("configSystemNumberParkA",$oData->numberParkA);
	   $tpl->set_var("configSystemNumberParkB",$oData->numberParkB);
	   $tpl->set_var("configSystemNumberNoteParkA",$oData->numberNoteParkA);
	   $tpl->set_var("configSystemNumberNoteParkB",$oData->numberNoteParkB);
	   $tpl->set_var("configSystemNumberDebitParkA",$oData->numberDebitParkA);
	   $tpl->set_var("configSystemNumberDebitParkB",$oData->numberDebitParkB);
	
	    $tpl->set_var("configSystemNumberRentA",$oData->numberRentA);
	   $tpl->set_var("configSystemNumberRentB",$oData->numberRentB);
	   $tpl->set_var("configSystemNumberNoteRentA",$oData->numberNoteRentA);
	   $tpl->set_var("configSystemNumberNoteRentB",$oData->numberNoteRentB);
	   $tpl->set_var("configSystemNumberDebitRentA",$oData->numberDebitRentA);
	   $tpl->set_var("configSystemNumberDebitRentB",$oData->numberDebitRentB);
	   
	}

public function getConceptTypes($tpl,$concept,$conceptType,$sPrefix){
		$vData = $concept->getConceptTypes();
		foreach($vData as $Item){
			if($conceptType == $Item->typeID){
				$tpl->set_var("sSelected","selected='selected'");
			}else{
				$tpl->set_var("sSelected","");		
			}
			$tpl->set_var("sConceptType",$Item->typeName);
			$tpl->set_var("conceptTypeID",$Item->typeID);

			$tpl->set_var("sNewConceptType",$Item->typeName);
			$tpl->set_var("newConceptTypeID",$Item->typeID);

			$tpl->set_var("sEditConceptType",$Item->typeName);
			$tpl->set_var("editConceptTypeID",$Item->typeID);

			$tpl->parse("ConceptTypeBlock",true);
			$tpl->parse("NewConceptTypeBlock",true);
			$tpl->parse("EditConceptTypeBlock",true);
		}
	}
	
	public function getConceptSalePoint($tpl,$concept,$conceptSalePoint,$sPrefix){
		$vData = $concept->getSalePoint();
		
		foreach($vData as $Item){
			if($conceptSalePoint == $Item->SalePoint){
				$tpl->set_var("sSelected","selected='selected'");
			}else{
				$tpl->set_var("sSelected","");		
			}
			
			//$tpl->set_var("sConceptTypeName",$Item->typeName);
			//$tpl->set_var("conceptSalePoint",$Item->SalePoint);

			$tpl->set_var("sNewConceptSalePointName",utf8_decode($Item->typeName));
			$tpl->set_var("iNewConceptSalePoint",$Item->SalePoint);

			$tpl->set_var("sEditConceptSalePointName",utf8_decode($Item->typeName));
			$tpl->set_var("iEditConceptSalePoint",$Item->SalePoint);

			//$tpl->parse("ConceptSalePointBlock",true);
			$tpl->parse("NewConceptSalePointBlock",true);
			$tpl->parse("EditConceptSalePointBlock",true);
		}
	}
	
	
	

	public function getConceptByType($tpl,$concept,$conceptType,$sPrefix){
		$vData = $concept->getConceptByType($conceptType);
		foreach($vData as $Item){
			$tpl->set_var("sNewMovementsConcept",$Item->typeName);
			$tpl->set_var("iNewMovementsConceptId",$Item->typeID);
			
			$tpl->parse("NewMovementsConceptBlock",true);
		}
	}
	


public function getConceptRates($tpl,$property){
		$vData = $property->getPropertyRates();
		foreach($vData as $Item){
			$tpl->set_var("sNewConceptPropertyType",$Item['description']);
			$tpl->set_var("iNewConceptPropertyTypeId",$Item['id']);
			
			$tpl->parse("NewConceptPropertyTypeBlock",true);

            $tpl->set_var("sEditConceptPropertyType",$Item['description']);
			$tpl->set_var("iEditConceptPropertyTypeId",$Item['id']);
			
			$tpl->parse("EditConceptPropertyTypeBlock",true);
		}
	}
	
	
public function getConceptStatus($tpl,$user){
		$vData = $concept->getConceptStatus();
		foreach($vData as $Item){
			if($Item->statusID != 1){ //No se muestra el estado eliminado en el alta
				$tpl->set_var("sNewConceptStatus",$Item->statusName);
				$tpl->set_var("newConceptStatusID",$Item->statusID);
				$tpl->parse("NewConceptStatusBlock",true);

				$tpl->set_var("sEditConceptStatus",$Item->statusName);
				$tpl->set_var("editConceptStatusID",$Item->statusID);
				$tpl->parse("EditConceptStatusBlock",true);
			}/*else{
				$tpl->set_var("sEditConceptStatus",$Item->statusName);
				$tpl->set_var("editConceptStatusID",$Item->statusID);
				$tpl->parse("EditConceptStatusBlock",true);
			}*/
		}
	}

public function showMovementsSearchForm($tpl,$meter,$property,$iPropertyId,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST){
		
		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("iUserID",$vPOST['iUserID']);
		$tpl->set_var("sMeterSerial",$vPOST['meterSerial']);
		$tpl->set_var("sMeterUser",$vPOST['meterUser']);
		$tpl->set_var("sMeterApple",$vPOST['meterApple']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}




	public function showMeterSearchForm($db,$tpl,$meter,$property,$user,$iPropertyId,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST){
		// METER STATUS
		$this->getMeterStatus($tpl,$meter,$iUserType,null);
		// PROPERTY TYPES
		$this->getPropertyTypes($tpl,$property);
		// USERS CATEGORY
		$this->getMeterUserCategories($tpl,$user);
		// USERS TYPE DOCUMENT
		$this->getMeterUserTypeDocument($tpl,$user);
		// PROPERTY RATES
		$this->getPropertyRates($tpl,$property);
		// PROVINCES
		$this->getProvinces($tpl,$db);
		// IMAGE
		if($iPropertyId && $iPropertyId!=0){
			$this->getPropertyImage($property,$iPropertyId);
		}
		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("iUserID",$vPOST['iUserID']);
		$tpl->set_var("sMeterSerial",$vPOST['meterSerial']);
		$tpl->set_var("sMeterUser",$vPOST['meterUser']);
		$tpl->set_var("sMeterApple",$vPOST['meterApple']);
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}

	public function showMeasureSearchForm($tpl,$meter,$property,$iPropertyId,$iTotPages, $iRecPerPage, $iMinLimit, $iPage, $vPOST){
		if(!$iTotPages || $iTotPages == 0){
			$tpl->set_var("NavigatorBlock","");
		}
		$tpl->set_var("iPage",$iPage);
		$tpl->set_var("rPerPage",$iRecPerPage);
		$tpl->set_var("sSelected".$iRecPerPage,"selected='selected'");
		$tpl->set_var("iMinLimit",$iMinLimit);	
		$tpl->set_var("iTotPages",$iTotPages);	
	}

	public function showMonthsBilling($tpl,$bill,$vPOST){
	
	
	}
	
	
	
	public function showMovementsNewForm($tpl,$concept,$vPOST){
	    
		// CONCEPT 
		$this->getConceptByType($tpl,$concept,1,null);
		// CONCEPT 	
	}
 
    public function tresnumeros($n, $last) {
		//global $numeros100, $numeros10, $numeros11, $numeros, $numerosX;
		if ($n == 100) return "cien ";
		if ($n == 0) return "cero ";
		$r = "";
		$cen = floor($n / 100);
		$dec = floor(($n % 100) / 10);
		$uni = $n % 10;
		if ($cen > 0) $r .= $this->numeros100[$cen] . " ";

		switch ($dec) {
			case 0: $special = 0; break;
			case 1: $special = 10; break;
			case 2: $special = 20; break;
			default: $r .= $this->numeros10[$dec] . " "; $special = 30; break;
		}
		if ($uni == 0) {
			if ($special==30);
			else if ($special==20) $r .= "veinte ";
			else if ($special==10) $r .= "diez ";
			else if ($special==0);
		} else {
			if ($special == 30 && !$last) $r .= "y " . $this->numerosX[$n%10] . " ";
			else if ($special == 30) $r .= "y " . $this->numeros[$n%10] . " ";
			else if ($special == 20) {
				if ($uni == 3) $r .= "veintitr�s ";
				else if (!$last) $r .= "veinti" . $this->numerosX[$n%10] . " ";
				else $r .= "veinti" . $this->numeros[$n%10] . " ";
			} else if ($special == 10) $r .= $this->numeros11[$n%10] . " ";
			else if ($special == 0 && !$last) $r .= $this->numerosX[$n%10] . " ";
			else if ($special == 0) $r .= $this->numeros[$n%10] . " ";
		}
		return $r;
	}
 
    public function seisnumeros($n, $last) {
		if ($n == 0) return "cero ";
		$miles = floor($n / 1000);
		$units = $n % 1000;
		$r = "";
		if ($miles == 1) $r .= "mil ";
		else if ($miles > 1) $r .= $this->tresnumeros($miles, false) . "mil ";
		if ($units > 0) $r .= $this->tresnumeros($units, $last);
		return $r;
	}
 
    public function docenumeros($n) {
		$nTmp = explode(".",$n);
		$n=(float)$n;
		if ($n == 0) return "cero ";
		$millo = floor($n / 1000000);
		$units = $n % 1000000;
		$r = "";
		if ($millo == 1) $r .= "un mill�n ";
		else if ($millo > 1) $r .= $this->seisnumeros($millo, false) . "millones ";
		if ($units > 0) $r .= $this->seisnumeros($units, true);
		if ($nTmp[1]) $r .= " con " . $this->seisnumeros((int)$nTmp[1], true) . " centavos";
		return $r;
    }
	
	
	
		public function encrypt($string, $key) {
   $result = '';
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)+ord($keychar));
      $result.=$char;
   }
   return base64_encode($result);
}
	
	
public function decrypt($string, $key) {
   $result = '';
   $string = base64_decode($string);
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)-ord($keychar));
      $result.=$char;
   }
   return $result;
}
	
		public function searchGlobal($query){
		$sSQL ="
				SELECT id,CONCAT(lastName,',',name) AS name,phone,address,email,cuit,document,1 as tipo 
				FROM `personal` WHERE lastName LIKE '%$query%' OR name LIKE '%$query%' OR phone LIKE '%$query%'
				OR address LIKE '%$query%' OR email LIKE '%$query%' OR cuit LIKE '%$query%' OR document LIKE '%$query%'
				UNION
				SELECT id,name,phone,address,email,cuit,cui as document,2 as tipo 
				FROM `school` WHERE name LIKE '%$query%' OR phone LIKE '%$query%'
				OR address LIKE '%$query%' OR email LIKE '%$query%' OR cuit LIKE '%$query%' OR cui LIKE '%$query%'	
			";
			
			//var_dump($sSQL);EXIT;
			$res=mysqli_query($sSQL);
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	
	
}
?>