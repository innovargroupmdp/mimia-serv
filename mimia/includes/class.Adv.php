<?php
class adv{
	

	
public function __construct(){
}	

	function wordlimit($string,$length)
{
    $words = explode(' ', $string);
    if (count($words) > $length)
    {
            return implode(' ', array_slice($words, 0, $length));
    }
    else
    {
            return $string;
    }
}


// Gets our data
public function fetchData($url){
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);
$result = curl_exec($ch);
curl_close($ch); 
return $result;
}
	
	public function getErrorMsj($errorCode, $sLang){

		$sSQL ="SELECT * FROM dictionary WHERE label = 'lbl_$errorCode' AND lang = '$sLang'";
		$res = mysql_query($sSQL);
		if($res){
			$row = mysql_fetch_assoc($res);
			if($row){
				return $row['description'];
			}else{
				return false;
			}
		}
	}
	
	/*Added By Sebastián Allende 2021*/
	public function searchGenericFieldByIdX($iId,$sField,$cnx){		


		$sSQL ="SELECT 	a.".$sField." as ".$sField." 										
						FROM adv a 						
						WHERE a.id = ".$iId." limit 1";
						
		//var_dump($sSQL);exit;
		$res=mysqli_query($cnx,$sSQL);
		
		//var_dump($sSQL);exit;
	
	    if ($res) {
				//$Data = new stdClass();
				$vData = mysqli_fetch_assoc($res);
				return $vData;	
		} 
		return null;		
	}

	/*	
		Busqueda generica agregado por Sebastian Allende
		Modificar para que detecte strigs en los campos de busqueda
		de lo contrio error!!!
	*/
	public function searchAdvByGenField($Field_1,$sSearch_1,$Field_2,$sSearch_2,$Field_3,$sSearch_3,$Logic,$cnx){
		
		$sWhere = null;
		$sSelect= null;
		if($Field_1){
			if(!$sWhere)				
					$sWhere=" WHERE a.$Field_1 = $sSearch_1";
			else					
					$sWhere.=" $Logic a.$Field_1 = $sSearch_1";									
			$sSelect=",a.$Field_1 ";
		}
		if($Field_2){
			if(!$sWhere)					
					$sWhere=" WHERE a.$Field_2 = $sSearch_2";					
			else				
				$sWhere.=" $Logic a.$Field_2 = $sSearch_2";							
			$sSelect.=",a.$Field_2 ";
		}
		if($Field_3){
			if(!$sWhere)				
				$sWhere=" WHERE a.$Field_3 = '".$sSearch_3."'";				
			else				
				$sWhere.=" $Logic a.$Field_3 = '".$sSearch_3."'";							
			$sSelect.=",a.$Field_3 ";
		}				
		$sSQL ="SELECT a.id $sSelect
				FROM adv a $sWhere";						
		//ORDER BY a.puesto DESC,a.id DESC";
		//echo ($sSQL);	
		try{						
			$res = mysqli_query($cnx,$sSQL);			
			$oData= new stdClass();
			if($res){				
				if ($row = mysqli_fetch_assoc($res)) {
					$oData->query=$row;
					$oData->queryStatus = "OK";
				}else
					$oData->queryStatus="ERR0030";	
				//var_dump($oData);exit;				
			}else{
				$oData->queryStatus="ERR0030";
			}
		}catch (Exception $e){
			$oData->queryStatus="ERR0030";
		}

		return $oData;		

	}

	public function searchAdvByIdX($iId,$cnx){

		
		try{
			$oData = new StdClass();
			$sImgIdx= $this->searchGenericFieldByIdX($iId,"imgIdx",$cnx);
			$sMediaIdx= $this->searchGenericFieldByIdX($iId,"mediaIdx",$cnx);				

			//var_dump($sImgIdx['imgIdx']);
			
			$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
					a.id, 
					a.name,
					a.cell,
					a.tari,
					a.llamada,
					a.beginDate,
					a.endDate,
					a.description,
					c.name as city,
					w.name as Kg,
					h.name as al,
					t.name as type,
					a.video as video";

			for ($i=1;($i<=intval($sImgIdx['imgIdx']))&&($i<100);$i++)
				$sSQL.=",a.img".$i;
			for ($i=1;($i<=intval($sMediaIdx['mediaIdx']))&&($i<100);$i++)
				$sSQL.=",a.media".$i;
			
			$sSQL.=",a.description		
					FROM adv a INNER JOIN city c ON a.city=c.id
					INNER JOIN weight w ON a.kg=w.id
					INNER JOIN height h ON a.al=h.id
					INNER JOIN type t ON a.type=t.id WHERE a.id= $iId AND a.status=1 AND a.endDate >= CURDATE() limit 1";
			//echo("<br>".intval($sImgIdx['imgIdx']));
			//echo("<br> $oData->imgIdx");
			//echo("<br>".intval($oData->imgIdx));	
			//echo("<br>".$sSQL);
			$res = mysqli_query($cnx,$sSQL);
			//var_dump($res);
			if ($res){
				$row = mysqli_fetch_assoc($res);
				//echo("row <br> ");
				//var_dump($row);
				if($row){
					$oData = $row;
					$oData['imgIdx'] = $sImgIdx['imgIdx'];
					$oData['mediaIdx'] = $sMediaIdx['mediaIdx'];
					$oData['queryStatus'] = "OK";
					//var_dump($oData);exit;
				}else{
					$oData['queryStatus']="ERR0030";
				}
			}	
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		
		return $oData;
		
	}


			//productos por búsqueda
		public function getProductsSearchId($name){
		try{
		$sSQL ="
			SELECT Distinct a.id, a.file, a.file1,a.envio,a.file2,a.file3,a.model as model,a.name as name,a.quantity,
			a.cuotas, a.description,a.value,a.codBar,a.cantidadCuotas,a.valorCuota,a.valorFinanciado,b.name as brand,CONCAT(t.cuota,' ',t.name,' DE $ ',round( ( round(a.value) + (round(a.value) * (t.rec/100))) / t.cuota )) as condicion
			FROM adv a 
			INNER JOIN brands b ON a.brand=b.id 
			INNER JOIN terms t ON a.conditionPay=t.id 
			WHERE a.status=1 AND a.id=$name AND t.visible=1 
			ORDER BY a.value asc limit 1";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			$row = mysql_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}


	public function get_Images(){
		$sSQL ="
			SELECT id, file	as name, description
			FROM adv where type=0 and status=2";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	public function get_Numero(){
		$sSQL ="
			SELECT (numero+1) as numero FROM `orders` WHERE type=0 order by numero desc limit 1";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData=$row['numero'];
				}
				return $vData;
			}
	}
	
	/*la modifique paa que que lea vectores sobre los campos
	  Soluciona el problema deprecado para PHP8 		*/
	public function registerVec($vData,$cnx){		
		if(!empty($vData)){
			$sValueList= "";
			$sFieldList= ""; 
			foreach($vData as $sField => $sValue){
				//echo($i++);
				//$sFieldList.= "`".$sField."`, ";								
				if (is_array($sValue)){
					foreach($sValue as $aField => $sValue1){
						//echo($aField);
						$sFieldList.= "".$sField.(1+$aField).", ";				
						if(is_int($sValue1) || is_float($sValue1)){
							$sValueList .= $sValue1.', ';
						}else{
							$sValueList .= '\''. rawurldecode($sValue1).'\', ';
						}
					}
				
				} else{ 					
					$sFieldList.= "".$sField.", ";
					if(is_int($sValue) || is_float($sValue)|| is_bool($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
						
				}
				
			}
			
			$sValueList = substr($sValueList,0,-2);
			$sFieldList = substr($sFieldList,0,-2);
			//echo($sFieldList);
			//	echo("\n");
			//	echo($sValueList);
			//	echo("\n");	
		}
		try{
			$oData = new StdClass();
			$sSQL ="INSERT INTO adv (".$sFieldList.") VALUES  (".$sValueList.")";
			//echo($sSQL);exit;
			$res=mysqli_query($cnx,$sSQL);
			if($res){
					$oData->status = "OK";
					$oData->lastId = mysqli_insert_id($cnx);
			}else{
				$oData->status = "ERR0007";
				echo mysqli_error($cnx);
			}
		}catch (Exception $e){
			$oData->status = "ERR0007";
			echo mysqli_error($cnx);
		}		
		//var_dump($sSQL);exit;
		return $oData;

	}

		public function add($idADV,$id){
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO movements
					(adv,user,date,paid,value,datePaid,pay)
					VALUES 
					($idADV,$id,'".date("Y-m-d")."', 0, (select value from parameters),'',0)";
					//var_dump($sSQL);
				$res=mysql_query($sSQL);
				if($res){
						$oData->status = "OK";
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			
		
		return $oData;
	}
	
	
	public function searchNewsAdv($iMinLimit, $iRecPerPage,& $iRecordsTotal){
		$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
		id,file,email, type FROM adv WHERE status=1";
		//var_dump($sSQL);exit;
		try{
			$res=mysql_query($sSQL);
			if($res){
				while ($row = mysql_fetch_array($res)){
					$oData = new stdClass();
					$oData->id 			= $row['id'];
					$oData->file 		= $row['file'];
					$oData->email 	= $row['email'];
					$oData->type 	= $row['type'];
					$vData[]=$oData;
				}
			}else{
				throw new Exception("ERR0006");
			}
		}catch (Exception $e){
			return $e->getMessage();
		}
		$sSQL ="SELECT FOUND_ROWS() as Total";
		$res=mysql_query($sSQL);
		$row=mysql_fetch_assoc($res);
		$iRecordsTotal=$row['Total'];
		return $vData;			
	}
	
	
	public function searchTotalAdv(){
		$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
		id FROM adv WHERE status=2";
		//var_dump($sSQL);exit;
		try{
			$res=mysql_query($sSQL);
			if($res){
				while ($row = mysql_fetch_array($res)){
					$oData = new stdClass();
					$oData->id 			= $row['id'];
					$vData[]=$oData;
				}
			}else{
				throw new Exception("ERR0006");
			}
		}catch (Exception $e){
			return $e->getMessage();
		}
		$sSQL ="SELECT FOUND_ROWS() as Total";
		$res=mysql_query($sSQL);
		$row=mysql_fetch_assoc($res);
		$iRecordsTotal=$row['Total'];
		return $vData;			
	}
	
	public function activate($iID, $activar){
		$sSQL ="UPDATE adv SET status = $activar WHERE id=$iID";
		$res = mysql_query($sSQL);
		if($res){
			return true;
		}else{
			return false;
		}
		
	}
	
	public function solicitar($iID){
		$sSQL ="UPDATE movements SET paid = 2 WHERE user = $iID AND paid = 0";
		$res = mysql_query($sSQL);
		if($res){
			return true;
		}else{
			return false;
		}
		
	}
	
	public function searchAdvertisingAdmin($searchName,$searchCity,$searchTari,$searchType,$searchDate,$searchStatus,$top,$cnx){
		$sWhere = null;
		if($searchName){
			if(!$sWhere){
				$sWhere=" WHERE a.name LIKE '%$searchName%'";
			}else{
				$sWhere.=" AND a.name LIKE '%$searchName%'";
			}
		}
		if($searchCity){
			if(!$sWhere){
				$sWhere=" WHERE a.city = $searchCity";
			}else{
				$sWhere.=" AND a.city = $searchCity";
			}
		}
		if($searchTari){
			if(!$sWhere){
				$sWhere=" WHERE a.tari = $searchTari";
			}else{
				$sWhere.=" AND a.tari = $searchTari";
			}
		}
		if($searchDate){
			if(!$sWhere){
				$sWhere=" WHERE a.endDate = '$searchDate'";
			}else{
				$sWhere.=" AND a.endDate = '$searchDate'";
			}
		}
		if($searchType){
			if(!$sWhere){
				$sWhere=" WHERE a.type = $searchType";
			}else{
				$sWhere.=" AND a.type = $searchType";
			}
		}	
		
		
		if($top){
			if(!$sWhere){
				$sWhere=" WHERE a.top = $top";
			}else{
				$sWhere.=" AND a.top = $top";
			}
		}	
		
		
		
		if($searchStatus){
			
			if($searchStatus==2){
				$pepe=0;
			} else {
				$pepe=$searchStatus;
			}
			
			if(!$sWhere){
				$sWhere=" WHERE a.status = $pepe";
			}else{
				$sWhere.=" AND a.status = $pepe";
			}
		}
		
		/*if(!$sWhere){
				$sWhere=" WHERE a.status = 1 AND a.endDate >= CURDATE()";
			}else{
				$sWhere.=" AND a.status = 1 AND a.endDate >= CURDATE()";
			}*/
		

		$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
		a.id, 
		a.name,
		a.cell,
		a.tari,
		a.description,
		c.name as city,
		w.name as Kg,
		h.name as al,
		t.name as type,
		a.img1,
		a.description,
		a.beginDate,
		a.endDate
		
		FROM adv a INNER JOIN city c ON a.city=c.id
		INNER JOIN weight w ON a.kg=w.id
		INNER JOIN height h ON a.al=h.id
		INNER JOIN type t ON a.type=t.id $sWhere";
		
		//var_dump($sSQL);exit;

	$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}
	
	
	
	public function searchAdvertisingAdminPayment($searchName,$searchCity,$searchTari,$searchType,$searchDate,$searchStatus,$cnx){
		$sWhere = null;
		if($searchName){
			if(!$sWhere){
				$sWhere=" WHERE a.name LIKE '%$searchName%'";
			}else{
				$sWhere.=" AND a.name LIKE '%$searchName%'";
			}
		}
		if($searchCity){
			if(!$sWhere){
				$sWhere=" WHERE a.city = $searchCity";
			}else{
				$sWhere.=" AND a.city = $searchCity";
			}
		}
		if($searchTari){
			if(!$sWhere){
				$sWhere=" WHERE a.tari = $searchTari";
			}else{
				$sWhere.=" AND a.tari = $searchTari";
			}
		}
		if($searchDate){
			if(!$sWhere){
				$sWhere=" WHERE a.endDate = '$searchDate'";
			}else{
				$sWhere.=" AND a.endDate = '$searchDate'";
			}
		}
		if($searchType){
			if(!$sWhere){
				$sWhere=" WHERE a.type = $searchType";
			}else{
				$sWhere.=" AND a.type = $searchType";
			}
		}	
		
		if($searchStatus){
			
			if($searchStatus==2){
				$pepe=0;
			} else {
				$pepe=$searchStatus;
			}
			
			if(!$sWhere){
				$sWhere=" WHERE a.status = $pepe";
			}else{
				$sWhere.=" AND a.status = $pepe";
			}
		}
		
		/*if(!$sWhere){
				$sWhere=" WHERE a.status = 1 AND a.endDate >= CURDATE()";
			}else{
				$sWhere.=" AND a.status = 1 AND a.endDate >= CURDATE()";
			}*/
		

		$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
		*
		
		FROM payments $sWhere";
		
		//var_dump($sSQL);exit;

	$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}
	
	public function searchAdvertisingX($start,$searchName,$searchCity,$searchTari,$searchAl,$searchKg,$searchType,$cnx){
		$sWhere = null;
		if($searchName){
			if(!$sWhere){
				$sWhere=" WHERE a.name LIKE '%$searchName%'";
			}else{
				$sWhere.=" AND a.name LIKE '%$searchName%'";
			}
		}
		if($searchCity){
			if(!$sWhere){
				$sWhere=" WHERE a.city = $searchCity";
			}else{
				$sWhere.=" AND a.city = $searchCity";
			}
		}
		if($searchTari){
			if(!$sWhere){
				$sWhere=" WHERE a.tari = $searchTari";
			}else{
				$sWhere.=" AND a.tari = $searchTari";
			}
		}
		if($searchAl){
			if(!$sWhere){
				$sWhere=" WHERE a.al = $searchAl";
			}else{
				$sWhere.=" AND a.al = $searchAl";
			}
		}
		if($searchKg){
			if(!$sWhere){
				$sWhere=" WHERE a.kg = $searchKg";
			}else{
				$sWhere.=" AND a.kg = $searchKg";
			}
		}
		if($searchType){
			if(!$sWhere){
				$sWhere=" WHERE a.type = $searchType";
			}else{
				$sWhere.=" AND a.type = $searchType";
			}
		}
		if(!$sWhere){
				$sWhere=" WHERE a.status = 1 AND a.endDate >= CURDATE()";
			}else{
				$sWhere.=" AND a.status = 1 AND a.endDate >= CURDATE()";
			}
		
		
		$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
		a.id,
		a.name,
		a.cell,
		a.tari,
		a.description,
		c.name as city,
		w.name as Kg,
		h.name as al,
		t.name as type,
		a.mediaIdx as mediaIdx,
		a.imgIdx as imgIdx,		
		a.llamada,
		a.description,
		a.video,
		a.top
		
		FROM adv a INNER JOIN city c ON a.city=c.id
		INNER JOIN weight w ON a.kg=w.id
		INNER JOIN height h ON a.al=h.id
		INNER JOIN type t ON a.type=t.id
		$sWhere
		ORDER BY a.top DESC,a.puesto DESC,a.id ASC LIMIT $start,25
		";
		
		//ORDER BY a.puesto DESC,a.id DESC";
		//var_dump($sSQL);exit;
		

		$res_1=mysqli_query($cnx,$sSQL);
		//var_dump($res_1);//exit;		
		if($res_1){
			$k=0;
			while($row = mysqli_fetch_assoc($res_1)){
				//var_dump($row);
				$vData[]=$row;
				$vData[$k]['mediaIdx']>1? $i=rand(1,$vData[$k]['mediaIdx']) : $i=1;
				$vData[$k]['imgIdx']>1  ? $j=rand(1,$vData[$k]['imgIdx']) : $j=1;
				$sSQL ="SELECT
						a.media".$i." as gif,
						a.img".$j." as img						
						FROM adv a WHERE a.id=".$vData[$k]['id'];
				
				$res_2= mysqli_query($cnx,$sSQL);
				//echo("<br>".$sSQL);
				if($res_2){					
					$row = mysqli_fetch_assoc($res_2);					
					$vData[$k]['gif']=$row['gif'];
					$vData[$k]['img']=$row['img'];				
					//var_dump($row);
				}

				$k++;
			}
			return $vData;
		}		
	}
	
	public function searchAdvertising($start,$searchName,$searchCity,$searchTari,$searchAl,$searchKg,$searchType,$cnx){
		$sWhere = null;
		if($searchName){
			if(!$sWhere){
				$sWhere=" WHERE a.name LIKE '%$searchName%'";
			}else{
				$sWhere.=" AND a.name LIKE '%$searchName%'";
			}
		}
		if($searchCity){
			if(!$sWhere){
				$sWhere=" WHERE a.city = $searchCity";
			}else{
				$sWhere.=" AND a.city = $searchCity";
			}
		}
		if($searchTari){
			if(!$sWhere){
				$sWhere=" WHERE a.tari = $searchTari";
			}else{
				$sWhere.=" AND a.tari = $searchTari";
			}
		}
		if($searchAl){
			if(!$sWhere){
				$sWhere=" WHERE a.al = $searchAl";
			}else{
				$sWhere.=" AND a.al = $searchAl";
			}
		}
		if($searchKg){
			if(!$sWhere){
				$sWhere=" WHERE a.kg = $searchKg";
			}else{
				$sWhere.=" AND a.kg = $searchKg";
			}
		}
		if($searchType){
			if(!$sWhere){
				$sWhere=" WHERE a.type = $searchType";
			}else{
				$sWhere.=" AND a.type = $searchType";
			}
		}
		if(!$sWhere){
				$sWhere=" WHERE a.status = 1 AND a.endDate >= CURDATE()";
			}else{
				$sWhere.=" AND a.status = 1 AND a.endDate >= CURDATE()";
			}
		

		$sSQL ="SELECT SQL_CALC_FOUND_ROWS   
		a.id,
		a.name,
		a.cell,
		a.tari,
		a.description,
		c.name as city,
		w.name as Kg,
		h.name as al,
		t.name as type,
		a.img1,
		a.llamada,
		a.description,
		a.video,
		a.top
		
		FROM adv a INNER JOIN city c ON a.city=c.id
		INNER JOIN weight w ON a.kg=w.id
		INNER JOIN height h ON a.al=h.id
		INNER JOIN type t ON a.type=t.id
		$sWhere
		ORDER BY a.top DESC,a.puesto DESC,a.id ASC LIMIT $start,25
		";
		
		//ORDER BY a.puesto DESC,a.id DESC";
		//var_dump($sSQL);//exit;

	$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}


	public function searchAdvertisingFullADV($cnx){
		
		$sSQL ="SELECT COUNT(*) as total_adv FROM adv  WHERE status = 1 AND endDate >= CURDATE()";

	$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}






	
	public function getProductsOffer22(){
		$sSQL ="
			SELECT id, file, file1,file2,file3,name,cuotas, description,value
			FROM adv where offer=1 and status=1";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while ($row = mysql_fetch_array($res)){
					$oData = new stdClass();
					$oData->id 			= $row['id'];
					$oData->file 		= $row['file'];
					$oData->file1 		= $row['file1'];
					$oData->file2		= $row['file2'];
					$oData->file3 		= $row['file3'];
					$oData->name 	= $row['name'];
					$oData->cuotas 	= $row['cuotas'];
					$oData->description 	= $row['description'];
					$vData[]=$oData;
				}
				return $vData;
			}
	}
	
		public function getProductsOffer(){
		$sSQL ="
			SELECT a.id, a.file, a.file1,a.file2,a.model,a.file3,a.name as name,a.cuotas, a.description,a.value,a.cantidadCuotas,a.valorCuota,a.valorFinanciado,b.name as brand
			FROM adv a INNER JOIN brands b ON a.brand=b.id where a.offer=1 and a.status=1 order by a.id desc limit 3";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
		public function getSubCategorys($cate){
		$sSQL ="
			SELECT id, lower(trim(description)) as description
			FROM subcategory where category = $cate";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
		public function getAdvAllNew($cnx){
		$sSQL ="
			select id
			FROM adv
			WHERE status=0";
			
			//var_dump($sSQL);exit;
			$res=mysqli_query($cnx,$sSQL);
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
			public function getAdvAll($cnx){
		$sSQL ="
			select id
			FROM adv
			WHERE status=1";
			
			//var_dump($sSQL);exit;
			$res=mysqli_query($cnx,$sSQL);
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	//productos por búsqueda
		public function getProductsSearch($name){
		$sSQL ="
			SELECT a.id, a.file, a.file1,a.file2,a.model,a.file3,a.name as name,a.cuotas, a.description,a.value,b.name as brand,a.quantity,
			a.cantidadCuotas,a.valorCuota,a.valorFinanciado
			FROM adv a 
			INNER JOIN brands b ON a.brand=b.id
			INNER JOIN terms t ON a.conditionPay=t.id 			
			WHERE a.status=1 AND (a.name like '%$name%' OR a.description like '%$name%' OR b.name like '%$name%') AND t.visible=1
			ORDER BY a.value asc";

			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	//productos por web en categoria y subcategoria
		public function getProductsByArt($name){
			
		$datos = explode("/", $name);

if ($datos[0]>=1 && $datos[0]<=8) {
	$Where ="WHERE a.status=1 and a.category = $datos[0] "; 
}
if ($datos[1]){
	$Where.="AND a.subcategory = $datos[1]";
}

			
		$sSQL ="
			SELECT Distinct a.id, a.file,a.model, a.file1,a.file2,a.file3,a.name as name,a.cuotas, a.description,a.value,a.cantidadCuotas,a.valorCuota,a.valorFinanciado,b.name as brand
			FROM adv a INNER JOIN brands b ON a.brand=b.id $Where ORDER BY a.value";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	

			public function getNameProductsByArt($name){
			
		$datos = explode("/", $name);

if ($datos[0]>=1 && $datos[0]<=8) {
	$Where ="WHERE a.id = $datos[0] "; 
}

$sub="";
if ($datos[1]){
	$sub=",(Select s.description FROM subcategory s Where s.id=$datos[1] and s.category=$datos[0]) as subcategoria";
}

			
		$sSQL ="
			SELECT Distinct a.description as categoria $sub
			FROM category a $Where";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	
	
	
	
	
	//submunu
	
			public function getProductsByArtSub($name){

if ($name>=1 && $name<=8) {
	$Where ="WHERE a.status=1 and a.category = $name "; 
}

			
		$sSQL ="
			SELECT Distinct a.id, a.file, a.file1,a.model as model,a.file2,a.file3,a.name as name,a.cuotas, a.description,a.quantity,
			b.name as brand,a.value,a.subcategory as category,a.cantidadCuotas,a.valorCuota,a.valorFinanciado
			FROM adv a INNER JOIN brands b ON a.brand=b.id $Where ORDER BY a.value";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
			//servicios tecnicos cargados
		public function getServicesOn(){
		
	$sSQL ="
			SELECT Distinct a.id,b.name as brand,a.name,a.address,a.observaciones,a.phone,a.cellPhone 
			FROM services a INNER JOIN brands b ON a.brand=b.id";
	

			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
			//servicios tecnicos cargados
		public function getServicesOnIn(){
		
	$sSQL ="
			SELECT Distinct a.id,b.name as brand,a.name,a.address,a.observaciones,a.phone,a.cellPhone 
			FROM services a INNER JOIN brands b ON a.brand=b.id
			";
	        //WHERE type=1

			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
		//servicios tecnicos cargados
		public function getServices($name){
		
if(is_numeric ($name)){
	
	$sSQL ="
			SELECT Distinct a.numero as id,concat('Orden: 0001-',' ',YEAR(NOW()),LPAD(a.numero,4,0)) as brand,
			concat('Marca: ',bra.name,'<br>','Articulo: ',a.articulo, '<br>',' Problema: ',a.problem,' ', a.descrip) as name,
			concat('Fecha Ingreso',' ',a.beginDate) as address,
			concat('<strong>Estado: </strong>',b.name) as observaciones,
			a.seguimiento as phone,
			a.articulo as phone1
			
			
			FROM orders  a INNER JOIN order_status b ON a.status=b.id 
			INNER JOIN brands bra ON a.marca=bra.id
			WHERE a.type=0 AND (a.dni=$name or a.numero=$name)";
	
} else {
	$sSQL ="
			SELECT Distinct a.id as id,b.name as brand,a.name as name,a.address as address,a.observaciones as observaciones,a.phone as phone,a.cellPhone as cellPhone 
			FROM services a INNER JOIN brands b ON a.brand=b.id WHERE a.type=0 AND b.name LIKE '%$name%'";
}

		
		
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	//productos totales
		public function getProducts(){
		$sSQL ="
			SELECT a.id, a.file, a.file1,a.file2,a.file3,a.model as model,a.name,a.cantidadCuotas as cuotas, 
			a.description,a.value,c.description as category,dateUpdate as sUpdate,a.codBar,a.quantity,
			s.description as subcategory
			,e.name as brand, if (a.status=1,'VISIBLE','NO VISIBLE') as visible,
			if(a.envio=1,'CONSULTAR ENVIO','ENVIO SIN CARGO') as envio,a.valorCuota,a.valorFinanciado
			FROM adv a INNER JOIN category c ON a.category=c.id INNER JOIN subcategory s ON a.subcategory=s.id
			INNER JOIN brands e ON a.brand=e.id ORDER BY e.name,a.name,a.value";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	//productos por id
	public function searchProductsById($iId,$cnx){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id, status,name,description,llamada,puesto,cell,email,video,top,city,tari,kg,al,type,img1,img2,img3,img4,img5,img6,img7,img8,endDate
			FROM adv  WHERE id=$iId";
				
			//var_dump($sSQL);exit;
			$res=mysqli_query($cnx,$sSQL);
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	
				public function newAdvUp($sNameUpdate){
				
		if($sNameUpdate){
			try{
				
				$oData = new StdClass();
				//aca comienza la lectura del archivo excell
				require_once 'Excel/reader.php';
				$data = new Spreadsheet_Excel_Reader();
				$data->setOutputEncoding('CP1251');
				//move_uploaded_file($sNameUpdate['sNameUpdate']['tmp_name'],'temp/products.xls');
				//var_dump($sNameUpdate);exit;
				//Ver donde esta el archivo temporal
				$data->read('temp/'.$sNameUpdate);
				error_reporting(E_ALL ^ E_NOTICE);

				
				for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
					
					if ($data->sheets[0]['cells'][$i][3]!='' && $data->sheets[0]['cells'][$i][7]!='') {
					
					$oData = new StdClass();
				    $sSQL ="
					INSERT INTO adv (description,file,name,file1,file2,file3,
					value,category,offer,cuotas,subcategory,cantidadCuotas,valorCuota,valorFinanciado,brand,status,model,quantity)
					VALUES ('".$data->sheets[0]['cells'][$i][1]."','".$data->sheets[0]['cells'][$i][2]."','".$data->sheets[0]['cells'][$i][3]."','".$data->sheets[0]['cells'][$i][4]."','".$data->sheets[0]['cells'][$i][5]."','".$data->sheets[0]['cells'][$i][6]."',".$data->sheets[0]['cells'][$i][7].",".$data->sheets[0]['cells'][$i][8].",
					".$data->sheets[0]['cells'][$i][9].",'".$data->sheets[0]['cells'][$i][10]."',".$data->sheets[0]['cells'][$i][11].
					",".$data->sheets[0]['cells'][$i][12].",".$data->sheets[0]['cells'][$i][13].",".$data->sheets[0]['cells'][$i][14].",".$data->sheets[0]['cells'][$i][15].
					",1,".$data->sheets[0]['cells'][$i][16].",".$data->sheets[0]['cells'][$i][17].")";
//var_dump($sSQL);exit;
				$res=mysql_query($sSQL);
					}
					}
					
					unlink('temp/'.$sNameUpdate);
					
				if($res){
					$oData->status = "OK";
				}else{
					$oData->status = "ERR0025";
				}
			}catch (Exception $e){
				$oData->status = "ERR0024";
			}
		} else {
		$oData = new StdClass();
		$oData->status = "ERR0024";
		}
		return $oData;
	}
	
	
	public function editProductsno($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'name'=>$vData->name,
			'description'=>$vData->description,
			'file'=>$vData->file,
			'file1'=>$vData->file1,
			'file2'=>'sinImagen.jpg',
			'file3'=>'sinImagen.jpg',
			'value'=>$vData->value,
			'cuotas'=>$vData->cuotas,
			'category'=>$vData->category,
			'valorCuota'=>$vData->valorCuota,
			'cantidadCuotas'=>$vData->cantidadCuotas,
			'valorFinanciado'=>$vData->valorFinanciado,
			'brand'=>$vData->brand,
			'offer'=>$vData->offer,
			'envio'=>$vData->envio,
			'status'=>$vData->status,
			'model'=>$vData->model,
			'llamada'=>$vData->llamada,
			'quantity'=>$vData->quantity,
			'conditionPay'=>$vData->conditionPay,
			'dateUpdate'=>$vData->dateUpdate,
			'codBar'=>$vData->codBar,
			'subcategory'=>$vData->subcategory); 
			
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE adv
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
				
				//var_dump($sSQL);exit;

			$res=mysql_query($sSQL);
			
			/*Var_dump($sSQL);
			Var_dump($res);
			exit;*/
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
		public function deleteProductsById($iId,$cnx){
		try{
			$oData = new StdClass();
			$sSqlArchivo = "select img1,img2,img3,img4,img5,img6,img7,img8 from adv where id = $iId";
			$res1=mysqli_query($cnx,$sSqlArchivo);
			$fila = mysqli_fetch_assoc($res1);
			
			$sSQL ="
				DELETE FROM adv WHERE id = $iId";
			$res=mysqli_query($cnx,$sSQL);
			if($res){
				if ($fila['img1']<>'nosignature.png'){
					unlink("img/adv/signature/".$fila['img1']);
				}
				if ($fila['img2']<>'nosignature.png'){
					unlink("img/adv/signature/".$fila['img2']);
				}
				if ($fila['img3']<>'nosignature.png'){
					unlink("img/adv/signature/".$fila['img3']);
				}
				if ($fila['img4']<>'nosignature.png'){
					unlink("img/adv/signature/".$fila['img4']);
				}
					if ($fila['img5']<>'nosignature.png'){
					unlink("img/adv/signature/".$fila['img5']);
				}
					if ($fila['img6']<>'nosignature.png'){
					unlink("img/adv/signature/".$fila['img6']);
				}
					if ($fila['img7']<>'nosignature.png'){
					unlink("img/adv/signature/".$fila['img7']);
				}
					if ($fila['img8']<>'nosignature.png'){
					unlink("img/adv/signature/".$fila['img8']);
				}
				
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
//slider todo	
	public function editSlider($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'name'=>$vData->name,
			'description'=>$vData->description,
			'img'=>$vData->img); 
			
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE slider
				SET ".$sValueList.
				" WHERE id = ".$vData->id;

			$res=mysql_query($sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function deleteSliderById($iId){
		try{
			$oData = new StdClass();
			$sSqlArchivo = "select img from slider where id = $iId";
			$res1=mysql_query($sSqlArchivo);
			$fila = mysql_fetch_assoc($res1);
			
			$sSQL ="
				DELETE FROM slider WHERE id = $iId";
			$res=mysql_query($sSQL);
			if($res){
				unlink("img_prod/slider/".$fila['img']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
		public function getSliderById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id,name,description,img
			FROM slider  WHERE id=$iId";
				
			//var_dump($sSQL);
			$res=mysql_query($sSQL);
			$row = mysql_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
		//sliders totales
		public function getSliders(){
		$sSQL ="
			SELECT id, name,description,img
			FROM slider";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
		public function newSlider($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO slider
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysql_query($sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysql_insert_id();
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			
		
		return $oData;
	}
	
//service todo

public function editProducts($vData,$cnx){

		if($vData){
			
		
			$vDataToUpdate = array(
			'name'=>$vData->name,
			'description'=>$vData->description,
			'cell'=>$vData->cell,
			'status'=>$vData->status,
			'puesto'=>$vData->puesto,
			'top'=>$vData->top,
			'city'=>$vData->city,
			'tari'=>$vData->tari,
			
			'kg'=>$vData->kg,
			'email'=>$vData->email,
			'llamada'=>$vData->llamada,
			'al'=>$vData->al,
			//'recibe'=>$vData->recibe,
			'type'=>$vData->type,
			'video'=>$vData->video,
			
			'endDate'=>$vData->endDate,
			
			'img1'=>$vData->img1,
			'img2'=>$vData->img2,
			'img3'=>$vData->img3,
			'img4'=>$vData->img4,
			'img5'=>$vData->img5,
			'img6'=>$vData->img6,
			'img7'=>$vData->img7,
			'img8'=>$vData->img8
			); 
			
			

			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE adv
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;

			$res=mysqli_query($cnx,$sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function editServiceMode($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'comentario'=>$vData->comentario); 
			
			

			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE orders
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;

			$res=mysql_query($sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	
	public function editServiceNext($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'status'=>$vData->status,
			'seguimiento'=>$vData->seguimiento,
			'comentario'=>$vData->comentario,
			'orden'=>$vData->orden,
			'endDate'=>$vData->endDate,
			'fechaSeguimiento'=>$vData->fechaSeguimiento,
			'servicio'=>$vData->servicio); 
			
			

			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE orders
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;

			$res=mysql_query($sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	
	
	
	
	public function deleteServiceById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysql_query($sSqlArchivo);
			$fila = mysql_fetch_assoc($res1);*/
			
			$sSQL ="
				DELETE FROM orders WHERE id = $iId";
			$res=mysql_query($sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
		public function getServiceById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id, name,numero,dni,articulo,lastName,phone,beginDate,problem,email,obser,descrip,status,endDate,marca,credito,fecha,serie,bill,modelo,recibe,
			seguimiento,servicio,comentario,orden,cellPhone,fechaSeguimiento
			FROM orders  WHERE id=$iId";
				
			//var_dump($sSQL);
			$res=mysql_query($sSQL);
			$row = mysql_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
		//sliders totales
		public function getService($n){
		$sSQL ="
			SELECT a.id, a.name,a.status as estado,a.lastName,a.cellPhone,a.articulo,a.phone,a.beginDate,
			a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,a.dni as dni,
			b.name as marca,a.credito,a.fecha,a.serie,a.modelo,a.numero
			FROM orders as a 
			INNER JOIN brands b ON a.marca=b.id 
			INNER JOIN order_status as c 
			ON a.status=c.id where a.type=$n ORDER BY a.beginDate ASC";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
		public function getServiceNews(){
		$sSQL ="
			SELECT a.id, a.name,a.numero,a.lastName,a.dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
			a.credito,a.fecha,a.serie,a.modelo
			FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id WHERE a.status=1 AND a.type=0 ORDER BY a.id DESC";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
		public function getServiceRepa(){
		$sSQL ="
			SELECT a.id, a.numero,a.name,a.lastName,a.dni as dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
			a.credito,a.fecha,a.serie,a.modelo as modelo,a.fechaSeguimiento,d.name as servicio, d.phone 
			FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id 
			INNER JOIN services d ON a.servicio=d.id WHERE a.status<>1 AND a.status=2 AND a.type=0 ORDER BY a.id DESC";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
			public function getServiceReparados(){
		$sSQL ="
			SELECT a.id, a.name,a.numero,a.lastName,a.dni as dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
			a.credito,a.fecha,a.serie,a.modelo as modelo,a.fechaSeguimiento,d.name as servicio, d.phone 
			FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id 
			INNER JOIN services d ON a.servicio=d.id WHERE a.status<>1 AND a.type=0 AND a.status=3  ORDER BY a.id DESC";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
			public function getServiceFina(){
		$sSQL ="
			SELECT a.id, a.name,a.numero,a.lastName,a.dni as dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
			a.credito,a.fecha,a.serie,a.modelo as modelo,a.fechaSeguimiento,d.name as servicio, d.phone 
			FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id 
			INNER JOIN services d ON a.servicio=d.id WHERE a.status<>1 AND a.type=0 AND a.status=4 ORDER BY a.id DESC";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
			public function getServiceDevo(){
		$sSQL ="
			SELECT a.id, a.name,a.numero,a.articulo,a.lastName,a.dni as dni,a.credito,a.bill,a.phone,a.beginDate,a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,b.name as marca,
			a.credito,a.fecha,a.serie,a.modelo as modelo,a.fechaSeguimiento,d.name as servicio, d.phone 
			FROM orders as a INNER JOIN brands b ON a.marca=b.id INNER JOIN order_status as c ON a.status=c.id 
			INNER JOIN services d ON a.servicio=d.id WHERE a.status<>1 AND a.type=0 AND a.status=5 ORDER BY a.id DESC";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	
	
		public function deletePedidoById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysql_query($sSqlArchivo);
			$fila = mysql_fetch_assoc($res1);*/
			
			$sSQL ="
				DELETE FROM pedido WHERE id = $iId";
			$res=mysql_query($sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
	
	
	
			public function getPedidoById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id, name,numero,dni,articulo,lastName,phone,beginDate,problem,email,obser,descrip,status,endDate,marca,credito,fecha,serie,bill,modelo,recibe,
			seguimiento,servicio,comentario,orden,cellPhone,fechaSeguimiento,
			(select nombre from parametros) as empresa,
(select domicilio from parametros) as address,
(select web from parametros) as web,
(select email from parametros) as emailEmpresa,
(select telefono from parametros) as phoneEmpresa,
(select logo from parametros) as logo	

			FROM pedido WHERE id=$iId";
				
			//var_dump($sSQL);
			$res=mysql_query($sSQL);
			$row = mysql_fetch_assoc($res);
			if($row){
				$oData = $row;
				if(count($this->getPedidosDetails($iId))>0){
					foreach ($this->getPedidosDetails($iId) as $ItemM){
					$detalle = $detalle . 'NOMBRE: '.$ItemM['cod'].'<br>'.$ItemM['size'].'<br>'.$ItemM['color'].'<br> CANTIDAD: '.$ItemM['quantity'].'<br> PRECIO UNIDAD: '.$ItemM['precioUnidad'].'<br> PRECIO TOTAL: '.$ItemM['total'].' <br><br>';
					} 
				} 
				$oData['articulos'] = $detalle;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	
	
	
public function editPedido($vData){

		if($vData){
			
			$vDataToUpdate = array(
			/*'name'=>$vData->name,
			'lastName'=>$vData->lastName,
			'phone'=>$vData->phone,
			'cellPhone'=>$vData->cellPhone,*/
			'status'=>$vData->status,
			/*'problem'=>$vData->problem,
			'descrip'=>$vData->descrip,
			'obser'=>$vData->obser,
			'email'=>$vData->email,
			'marca'=>$vData->marca,
			'recibe'=>$vData->recibe,
			'credito'=>$vData->credito,
			'serie'=>$vData->serie,
			'bill'=>$vData->bill,
			'dni'=>$vData->dni,
			'modelo'=>$vData->modelo,
			'articulo'=>$vData->articulo,
			'fecha'=>$vData->fecha,*/
			'comentario'=>$vData->comentario
			); 
			
			
//'endDate'=>$vData->endDate
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE pedido
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;

			$res=mysql_query($sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	
	
	
	
				public function getPedidosDetails($id){
		$sSQL ="
			SELECT id, cod,size,color,precioUnidad,total,quantity
			FROM pedido_next where idOrden = $id";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	
	
	
			public function getPedidos($type){
				
				if ($type==1) {
					$sWhere = " WHERE c.id=1 OR c.id=3";
				} else {
					$sWhere = " WHERE c.id in (2,4,5,6) ";
				}
				
		$sSQL ="
			SELECT a.id, a.name,a.status as estado,a.lastName,a.cellPhone,a.articulo,a.phone,a.beginDate,
			a.problem,a.email,a.obser,a.descrip,c.name as status,a.endDate,a.dni as dni,
			a.credito,a.fecha,a.serie,a.modelo,a.numero
			FROM pedido as a 
			INNER JOIN pedido_status as c			
			ON a.status=c.id $sWhere ORDER BY a.beginDate ASC";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
		
		public function getStatusPedido(){
		$sSQL ="
			SELECT id,name
			FROM pedido_status order by id";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	
	
	
	
		public function getStatus(){
		$sSQL ="
			SELECT id,name
			FROM order_status order by id";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
			public function updatePedido($iID, $activar){
		$sSQL ="UPDATE pedido SET dni = $activar WHERE id=$iID";
		$res = mysql_query($sSQL);
		if($res){
			return true;
		}else{
			return false;
		}
		
	}
	
	
		public function newPedido($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO pedido
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysql_query($sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysql_insert_id();
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			
		
		return $oData;
	}
	
	
		public function newPedidoMovements($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO pedido_next
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysql_query($sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysql_insert_id();
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			
		
		return $oData;
	}
	
	
	
	
	
	
	
	
	
	
	
		public function newService($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO orders
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysql_query($sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysql_insert_id();
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			
		
		return $oData;
	}
	
	
	
		public function searchOrderByIdOrder($iId){
		

	$sSQL ="SELECT 
					a.id,
					a.numero,
					CONCAT(a.lastName,' ',a.name) as userName,
					a.dni,
					a.articulo as articulo,
					a.phone,
					a.beginDate,
					a.problem,
					a.email,
					a.obser,
					a.descrip,
					c.name as status,
					a.endDate,
					b.name as marca,
					a.credito,
					a.fecha,
					a.serie,
					a.bill,
					a.modelo,
					a.recibe,
					a.seguimiento,
(select nombre from parametros) as empresa,
(select domicilio from parametros) as address,
(select web from parametros) as web,
(select email from parametros) as emailEmpresa,
(select telefono from parametros) as phoneEmpresa,
(select logo from parametros) as logo					
					

					FROM orders a 
					INNER JOIN brands b ON a.marca = b.id 
					INNER JOIN order_status c ON a.status = c.id 
					WHERE a.id = $iId limit 1";
					
	//var_dump($sSQL);exit;
	$res=mysql_query($sSQL);
	
	//var_dump($sSQL);exit;
	
	
	
	    if ($res) {
									$oData = new stdClass();
									$oData = mysql_fetch_assoc($res);
									return $oData;
	
			} 
		
	}
	
	
	
	
	
	
	
	
	
	
	public function newServices($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO services
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysql_query($sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysql_insert_id();
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			
		
		return $oData;
	}
	
	
	
	
	
	
	public function editServices($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'name'=>$vData->name,
			'phone'=>$vData->phone,
			'cellPhone'=>$vData->cellPhone,
			'email'=>$vData->email,
			'observaciones'=>$vData->obser,
			'brand'=>$vData->brand,
			'type'=>$vData->type,
			'address'=>$vData->address); 
			

			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE services
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function deleteServicesById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysql_query($sSqlArchivo);
			$fila = mysql_fetch_assoc($res1);*/
			
			$sSQL ="
				DELETE FROM services WHERE id = $iId";
			$res=mysql_query($sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
		public function getServicesById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id,name,address,observaciones,phone,brand,email,cellPhone,type
			FROM services  WHERE id=$iId";
				
			//var_dump($sSQL);
			$res=mysql_query($sSQL);
			$row = mysql_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	
	public function newMarca($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO brands
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysql_query($sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysql_insert_id();
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			
		
		return $oData;
	}
	
	public function newCondicion($vData){
			if($vData){
				foreach($vData as $sField => $sValue){
					$sFieldList.= "`".$sField."`, ";
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sValue.', ';
					}else{
						$sValueList .= '\''. rawurldecode($sValue).'\', ';
					}
				}
				$sValueList = substr($sValueList,0,-2);
				$sFieldList = substr($sFieldList,0,-2);
			}
			try{
				$oData = new StdClass();
				$sSQL ="
					INSERT INTO terms
					(".$sFieldList.")
					VALUES 
					(".$sValueList.")";
					//var_dump($sSQL);exit;
				$res=mysql_query($sSQL);
				if($res){
						$oData->status = "OK";
						$oData->lastId = mysql_insert_id();
				}else{
					$oData->status = "ERR0007";
				}
			}catch (Exception $e){
				$oData->status = "ERR0007";
			}
			
		
		return $oData;
	}
	
		public function editCondicion($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'name'=>$vData->name,
			'rec'=>$vData->rec,
			'cuota'=>$vData->cuota,
			'visible'=>$vData->visible
			); 
			
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE terms
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function editMarca($vData){

		if($vData){
			
			$vDataToUpdate = array(
			'name'=>$vData->name); 
			
			
			foreach($vDataToUpdate as $sField => $sValue){
					if(is_int($sValue) || is_float($sValue)){
						$sValueList .= $sField." = ".$sValue.', ';
					}else{
						$sValueList .= $sField." = ".'\''. rawurldecode($sValue).'\', ';
					}
				
			}
			$sValueList = substr($sValueList,0,-2);

		}
		try{
     
			$oData = new StdClass();
			$sSQL ="
				UPDATE brands
				SET ".$sValueList.
				" WHERE id = ".$vData->id;
//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
            
			if($res){
					$oData->status = "OK";
				
			}else{
				$oData->status = "ERR0037";
			}
		}catch (Exception $e){
			$oData->status = "ERR0037";
		}
		//var_dump('entro aca fin que');
		return $oData;
	}
	
	
	public function deleteMarcaById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysql_query($sSqlArchivo);
			$fila = mysql_fetch_assoc($res1);*/
			
			$sSQL ="
				DELETE FROM brands WHERE id = $iId";
			$res=mysql_query($sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
		public function deleteCondicionById($iId){
		try{
			$oData = new StdClass();
			/*$sSqlArchivo = "select mensaje from informacion where id = $iId";
			$res1=mysql_query($sSqlArchivo);
			$fila = mysql_fetch_assoc($res1);*/
			
			$sSQL ="
				DELETE FROM terms WHERE id = $iId";
			$res=mysql_query($sSQL);
			if($res){
				//unlink("information/".$fila['mensaje']);
				$oData->status="OK";
			}else{
				$oData->status="ERR0011";
			}
		}catch (Exception $e){
			$oData->status="ERR0011";
		}
		return $oData;
		
	}
	
		public function getMarcaById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id,name 
			FROM brands  WHERE id=$iId";
				
			//var_dump($sSQL);
			$res=mysql_query($sSQL);
			$row = mysql_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
		public function getCondicionById($iId){
		try{
			$oData = new StdClass();
			$sSQL ="SELECT id,name,rec,visible,cuota
			FROM terms  WHERE id=$iId";
				
			//var_dump($sSQL);
			$res=mysql_query($sSQL);
			$row = mysql_fetch_assoc($res);
			if($row){
				$oData = $row;
				$oData['queryStatus'] = "OK";
				//var_dump($oData);exit;
			}else{
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	public function getMarcas(){
		$sSQL ="
			SELECT id, name
			FROM brands";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
		public function getCondiciones($con){
			
		$Where=null;	
		if ($con=1) {
	$Where ="WHERE visible=1 ORDER BY cuota"; 
}	
			
			
			
		$sSQL ="
			SELECT id, name,rec,if(visible=1,'VISIBLE','NO VISIBLE') as visible,cuota
			FROM terms $Where";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	public function getCategorias(){
		$sSQL ="
			SELECT id, description
			FROM category";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}
	}
	
	
	public function getSubCategorias($q){
		$sSQL ="
			SELECT id, description
			FROM subcategory WHERE category=$q";
			
			//var_dump($sSQL);exit;
			$res=mysql_query($sSQL);
			$i=0;
			if($res){
				while($row = mysql_fetch_assoc($res)){
					$oData[$i]['id'] = $row['id'];
					$oData[$i]['description'] = $row['description'];
				
				$i++;
				}
				return $oData;
			}
			else{
			return null;
		}
	}
	
	
			//buscamos niveles por id
	public function editConceptById($iId,$val){
		try{
			//$oData = new StdClass();
			$valor=floatval($val);
			$newDate=date("Y-m-d"); 
			
				$sSQL ="UPDATE adv SET  
				value=".$valor.",
				dateUpdate='".$newDate."'
				WHERE id = $iId";
			
			
				
//var_dump($sSQL);
			$res=mysql_query($sSQL);
			//$row = mysql_fetch_assoc($res);
			if($res){
				//$oData = $row;
				$oData['queryStatus'] = "OK";
				$oData['dateUpdate'] = $newDate;
				
				//var_dump($oData);exit;
			}else{
				
				$oData['queryStatus']="ERR0030";
			}
		}catch (Exception $e){
			//echo 'error';
			$oData['queryStatus']="ERR0030";
		}
		return $oData;
		
	}
	
	
	
	
	
}	
?>