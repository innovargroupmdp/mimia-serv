<?php
class DB{
	public function __construct(){
	}
	
	public function getLabel($sLabel, $sLang,$cnx){
		$sSQL ="SELECT * FROM dictionary WHERE label = '$sLabel' AND lang = '$sLang'";
		//var_dump($sSQL);exit;
		try{
			$res = mysqli_query($cnx,$sSQL);
			if($res){
				$row = mysqli_fetch_assoc($res);
			}else{
				throw new Exception("ERR0001");
			}
			return $row['description'];
		}catch (Exception $e){
			return $e->getMessage();
		}
	}
	
	public function getGeoData($sZip,$cnx){
		if($sZip){
			$sSQL ="SELECT c.name AS cityName, p.name AS provinceName 
					FROM cities c
					INNER JOIN provinces p ON c.id_province = p.id
					WHERE zip = $sZip";
				//var_dump($sSQL);exit;
			$res=mysqli_query($cnx,$sSQL);
			
			$row = mysqli_fetch_assoc($res);
			if($row){
				$oData = new stdClass();
				$oData->city = $row['cityName'];
				$oData->province = $row['provinceName'];
				return $oData;	
			}else{
				return null;
			}
		}
	}
	
	public function getCities($cnx){
		$sSQL ="SELECT c.name, c.id, c.id_province, c.zip
				FROM cities c ";
			//var_dump($sSQL);exit;
		$res=mysqli_query($cnx,$sSQL);
		
		$row = mysql_fetch_array($res);
		if($row){
			while ($row = mysql_fetch_array($res)){
				$oData = new stdClass();
				$oData->id = $row['id'];
				$oData->name = $row['name'];
				$oData->idProvince = $row['id_province'];
				$oData->zip = $row['zip'];
				$vData[]=$oData;
			}
			return $vData;			
		}else{
			return null;
		}
	}

	public function getCitiesByProvince($iId_province,$cnx){
		$sSQL ="SELECT c.name, c.id, c.id_province, c.zip
				FROM cities c 
				WHERE c.id_province = $iId_province
				ORDER BY c.name";
			//var_dump($sSQL);exit;
		$res=mysqli_query($cnx,$sSQL);
		
		if($res){
			$i=0;
			while ($row = mysqli_fetch_assoc($res)){
				//$oData = array();
				$oData[$i]['id'] = $row['id'];
				$oData[$i]['name'] = $row['name'];
				$oData[$i]['idProvince'] = $row['id_province'];
				$oData[$i]['zip'] = $row['zip'];
				$i++;
			}
			return $oData;			
		}else{
			return null;
		}
	}
	
	//obtengo todas las provincias	
	public function getProvinces($cnx){
		$sSQL ="SELECT * FROM provinces";
		$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}
	
	
	public function getProvincesbyId($iId,$cnx){
		$sSQL ="SELECT `name` FROM provinces WHERE id = $iId";
		$res=mysqli_query($cnx,$sSQL);
		$row=mysqli_fetch_assoc($res);
		
		return $row['name'];
	}
	
	public function getCitiesbyId($iId,$cnx){
		$sSQL ="SELECT `name` FROM cities WHERE id = $iId";
		$res=mysqli_query($cnx,$sSQL);
		$row=mysqli_fetch_assoc($res);
		
		return $row['name'];
	}
	
	
		//obtengo todas tipos de documentos
	public function getPersonalDocument($cnx){
		$sSQL ="SELECT * FROM personal_typedocument";
		$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}
	
		//nombre de un cargo relacion familiar
			
	public function chargePersonalName($id,$cnx){
		$sSQL ="SELECT name FROM personal_charges WHERE id=$id limit 1";
		$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
			
	}
	
	//obtengo todas tipos de personas a cargo
	public function getPersonalCharges($cnx){
		$sSQL ="SELECT * FROM personal_charges";
		$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}
	
	//obtengo todas tipos de cargos afip
	public function getPersonalCategories($cnx){
		$sSQL ="SELECT * FROM personal_categories";
		$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}
	
	public function getPersonalCategoriesbyId($iId,$cnx){
		$sSQL ="SELECT `name` FROM personal_categories WHERE id = $iId";
		$res=mysqli_query($cnx,$sSQL);
		$row=mysqli_fetch_assoc($res);
		
		return $row['name'];
	}

	
		//obtengo todas los estados civiles
	public function getPersonalCivil($cnx){
		$sSQL ="SELECT * FROM personal_civil";
		$res=mysqli_query($cnx,$sSQL);
		
			if($res){
				while($row = mysqli_fetch_assoc($res)){
					$vData[]=$row;
				}
				return $vData;
			}		
	}
	
	public function getPersonalCivilbyId($iId,$cnx){
		$sSQL ="SELECT `name` FROM personal_civil WHERE id = $iId";
		$res=mysqli_query($cnx,$sSQL);
		$row=mysqli_fetch_assoc($res);
		
		return $row['name'];
	}
	
	
}
?>